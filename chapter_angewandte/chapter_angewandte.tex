\chapter{The competition between two-electron reduction products}
\label{angewandte}

%% The following annotation is customary for chapter which have already been
%% published as a paper.
%\blfootnote{}

\begin{abstract}
    Elucidating the pathways involved in the electrochemical reduction of carbon dioxide (e\ce{CO2}R) is crucial for the advancement of sustainable chemical and fuel manufacturing processes. Ag is an appealing e\ce{CO2}R catalyst due to its promising performance for carbon monoxide (\ce{CO}) production and relatively low cost. In this chapter, we study the role of the formate (\ce{HCOO^-}) intermediate $*$\ce{OCHO}, aiming to resolve the discrepancy between the current theoretical understanding and experimental performance of Ag catalysts. We demonstrate that the first coupled proton-electron transfer (CPET) step in the pathway for \ce{CO} production to form $*$\ce{COOH} competes with the Volmer step for formation of $*$\ce{H}, whereas this Volmer step is a prerequisite for the formation of the $*$\ce{OCHO} species. We show that $*$\ce{OCHO} should form readily on the Ag surface owing to substantial stabilization of the transition state due to solvation and favorable binding strength. In addition, we use in-situ surface-enhanced Raman spectroscopy (SERS) experiments to provide preliminary evidence of the presence of O-bound bidentate species on polycrystalline Ag catalyst during e\ce{CO2}R at low overpotentials which we attribute to $*$\ce{OCHO}. Lateral adsorbate-adsorbate interactions in the presence of $*$\ce{OCHO} have a significant influence on the surface coverage of $*$\ce{H}, resulting in the inhibition of \ce{HCOO^-} and \ce{H2} production and a higher selectivity towards \ce{CO}. We argue that the species $*$\ce{OCHO} plays an important role in determining the activity and selectivity for not only formate-producing catalysts but also for catalysts such as Ag that predominantly produce \ce{CO}. 
\end{abstract}

\vfill 

\noindent This chapter has been published in Angew. Chem. Int. Ed. \textbf{58}, 1345 (2019) \cite{Divya_angewandte}.

\bigskip

\noindent Computational research work led by Divya Bohra and supervised by Guanna Li and Evgeny A. Pidko; Experimental research work led by Isis Ledezma-Yanez and supervised by Wiebren de Jong; Wilson A. Smith supervised the project as the principal investigator.

\newpage

\section{Introduction}

The electrochemical reduction of \ce{CO2} is a very promising approach providing a means to manage intermittent renewable electricity production by converting it to a chemically valuable form, while recycling climate change-inducing \ce{CO2} \cite{Lu2016, Whipple2010,Gattrell2007,IPCC2014,Tryk2001}. Understanding the pathways for the (electro)chemical transformations involved in e\ce{CO2}R is critical to advance its technological utilization. The 2 proton-electron transfer products of e\ce{CO2}R, namely \ce{CO} and \ce{HCOO^-} are highly attractive due to relatively low overpotentials needed to drive their production, and high achievable Faradaic efficiencies \cite{Verma2016,Koper2011,Durst2015,Centi2009,Oloman2008,Song2006}. The need to balance the performance with low cost electrodes has led to an increased interest in using Ag as a \ce{CO2} reduction catalyst, which has a high selectivity to \ce{CO} while being 100 times cheaper than the alternative Au. \cite{Verma2016,Jaramillo2014,Ming2016,Ali2018,Yogi2016,Jiao2015,Kim2015,Rosen2015}. 

\medskip

It is widely accepted that the formation of \ce{CO} from \ce{CO2} on transition metal catalysts with an aqueous electrolyte proceeds via the $*$\ce{COOH} species, whereas the formation of \ce{HCOO^-} proceeds via the bidentate O-bound $*$\ce{OCHO} species, both forming after a single CPET step \cite{Appel2013,Jaramillo2014,Kortlever2015,Yoo,Feaster2017,Bagger2017}. Figure \ref{fig:111_211_110} in the Appendix shows a comparison between the adsorption energy of intermediates for \ce{CO} and \ce{HCOO^-} formation pathways for (111), (211) and (110) Ag surfaces. Limiting potentials ($U_L$) in all free energy plots are given vs. RHE and are defined as the minimum potential at which all reaction steps of a pathway are exergonic. As found experimentally \cite{Hoshi1997}, (110) is the most active surface for formation of \ce{CO} due to higher binding strength of $*$\ce{COOH} leading to a lower $U_L$. The opposite is true for formation of \ce{HCOO^-} for which the (111) surface has the least $U_L$ due to lower binding strength of $*$\ce{OCHO}. This implies that as the surface becomes more open and uncoordinated such as for (110), the relative $U_L$ for formation of \ce{CO} and \ce{HCOO^-} are closer to each other whereas formation of \ce{HCOO^-} has a drastically lower $U_L$ relative to \ce{CO} for close-packed surfaces such as (111). The surface structure of the catalyst is therefore expected to play an important role in determining selectivity between the e\ce{CO2}R products on Ag. 

\medskip

Ag(110) is used as the model surface for this chapter due to its high activity for the selective production of \ce{CO}. In addition to $*$\ce{COOH} as a precursor to \ce{CO}, we also consider the free energy of formation of $*$\ce{CO}+$*$\ce{OH} by the breaking of the \ce{C-O} bond as an alternative. However, we find that the limiting potential ($U_L$) for this step (green pathway in Figure \ref{fgr:2e}) is significantly higher than for the pathway via $*$\ce{COOH} (red pathway in Figure \ref{fgr:2e}).

\medskip

Consideration of the relative limiting potentials ($U_L$) alone dictates that the formation of \ce{H2} is most thermodynamically feasible on Ag(110), followed by \ce{HCOO^-} and then \ce{CO} as depicted in the free energy plot in Figure \ref{fgr:2e}. Interestingly, this conclusion does not reconcile with the experimental observations of the formation of \ce{CO} as the major product of e\ce{CO2}R on Ag, \ce{H2} as a by-product (through the hydrogen evolution reaction, HER), with the detection of only trace amounts of \ce{HCOO^-} for intermediate applied potentials of ca. -0.9 V to -1.3 V \cite{Jaramillo2014}. For applied potentials more or less negative of this range, the Faradaic efficiency for \ce{H2} dominates that for \ce{CO}. However, irrespective of the applied potential, the experimentally observed Faradaic efficiency for the formation of \ce{HCOO-} remains significantly lower (<10\%) relative to \ce{CO} and \ce{H2}. The reasons for this discrepancy and the role of the stable species $*$\ce{OCHO} in the catalytic performance of Ag is not well understood. 

\begin{figure}
\centering
\includegraphics[scale=0.65]{chapter_angewandte/figures/SI2_free_energy_plot_110_CO_HCOOH_H2_csm.pdf}
\caption{Free energy diagram after correcting for solvation of 2 electron transfer reactions on an Ag(110) surface at 0 V vs. the reversible hydrogen electrode (RHE). Limiting potentials ($U_L$) in the legend are given vs. RHE.}
\label{fgr:2e}
\end{figure}

\medskip

Here we report our findings of the mechanistic differences in the formation of $*$\ce{OCHO} and $*$\ce{COOH} and their respective interactions with the \ce{H2} production pathways on an Ag(110) surface. We show that there are two major bifurcations in the reaction mechanism before and after formation of $*$\ce{H} that control the selectivity between \ce{CO}, \ce{HCOO^-} and \ce{H2}. We present reaction barrier calculations to show that the formation of $*$\ce{OCHO} has a significantly lower kinetic barrier relative to $*$\ce{COOH} on Ag(110) and that solvation of the transition state by the surrounding water molecules plays an important role in determining this barrier. Finally, we demonstrate that the influence of lateral adsorbate-adsorbate interactions resulting from the presence of $*$\ce{OCHO} on the surface promotes \ce{CO} production while inhibiting itself and the formation of \ce{HCOO^-} as a consequence.

\medskip

\section{Computational Details}

A 3x3x4 slab size was used for Ag(111) and Ag(211) and a 2x3x6 slab size was used for Ag(110). Intermediate adsorption energies were calculated for all high symmetry binding sites on each surface and the binding energy found to be most favorable was used in the final plots. The Ag metal slab and surfaces with and without adsorbed intermediates were created using the Atomic Simulation Environment (ASE) \cite{Bahn2002}.  All density functional theory (DFT) calculations were carried out with the projector augmented wave (PAW) method as implemented in the GPAW software \cite{Enkovaara2010,Mortensen2005}. BEEF-vdW exchange correlation (xc) functional was used to perform all the energy calculations \cite{BEEF2012,Wellendorff2015} with plane-wave (PW) cutoff energy value of 450 eV. Lattice constant for the bulk FCC metal were calculated using the same xc functional and  PW cutoff for a residual force on all atoms of less than 0.005 eV \r{A}$^{-1}$. The resulting lattice constant for Ag, 4.1384 \r{A}, was used for all other energy calculations.

\medskip

For the geometry optimization of the  bare and intermediate adsorbed Ag slab, the top two metal layers as well as the adsorbed atoms were allowed to relax until a residual force of less than 0.01 eV \r{A}$^{-1}$ is reached. The self-consistent field (SCF) convergence criterion was set to 5 x $10^{-4}$ eV for adsorption energy calculations. A 12  \r{A}\ vacuum layer was placed above the periodically repeating slabs. The Fermi-Dirac method was used to smear the Fermi level with an electronic temperature of 0.1 eV and a Pulay mixing of the resulting electronic density was applied. A (3x3x1) k-point sampling was used and a grid spacing between 0.16 \r{A}\ and 0.2 \r{A}\ was used for all calculations.  Barrier energy calculations were performed using climbing image nudged elastic band (CI-NEB) method \cite{Henkelman2000} for which the SCF criterion of 1 x $10^{-5}$ eV and a maximum residual force of 0.05 eV \r{A}$^{-1}$ was used. Bader charge analysis has been performed using the code developed by the Henkelman group \cite{Henkelman2006,Sanville, Yu_Bader,Tang2009}.

\medskip

Vibrational analysis was performed at room temperature (298.15 K) using the harmonic approximation followed by a statistical mechanical treatment to calculate ensemble energies from single molecule energies \cite{Cramer2004}. The equation (\ref{eq:G}) provides the expression for obtaining free energy of a slab with adsorbed molecule $X$ (denoted as $*X$). The energies obtained using DFT ($E^{DFT}$) are corrected in this way for zero point vibrational energy ($E^{ZPVE}$), enthalpy ($\int_{T=0}^{T} C_{p}dT$) and vibrational entropy ($TS^{vib}$). $E_{xc}$ signifies the systematic errors associated with the xc functional BEEF-vdW \cite{Chan2014} and $E_{solv}$ indicates the solvation correction due to presence of water. The general expression (\ref{eq:Gads}) gives the free energies of adsorption ($\Delta G_{ads}$) of intermediate $*X$. $E_{xc}$ for BEEF-vdW was taken as 0.33 eV for the \ce{CO2} and \ce{HCOOH} molecules and 0.09 eV for the \ce{H2} molecule \cite{Studt2013}. Fugacities of the non-adsorbed species have been adapted from \citeauthor{Chan2014} \cite{Chan2014}. The free energy of deprotonation of \ce{HCOOH(aq)} is calculated as -0.19 eV according to the equation (\ref{eq:deprot}) assuming a formate concentration of 0.01 M, $pK_a$ of 3.79 and a pH of 6.8 \cite{Hansen2016}.

\[* + X(g) \rightarrow *X\]

\begin{equation}
G_{*X} = E^{DFT}_{*X} + E^{ZPVE}_{*X} + \int_{T=0}^{T} C_{p*X}dT - TS^{vib}_{*X} - E_{solv} + E_{xc}
\label{eq:G}
\end{equation}

\begin{equation}
\Delta G^{ads}_{*X} = G_{*X} - G_{slab} - G_{X(g)}
\label{eq:Gads}
\end{equation}

\begin{equation}
\Delta G^{deprot}_{HCOOH} = k_BT\{ln(10)*(pK_a -pH) + ln[HCOO^-]\}%kBT ln(10)(pKa   pH) + kBT ln[OCHO ]
\label{eq:deprot}
\end{equation}

\medskip

$E_{solv}$ for adsorbed species has been calculated using implicit solvation model VASPSol \cite{Fishman2013,Mathew2014} implemented in Vienna Ab Initio Simulation Package (VASP) \cite{VASP1,VASP2} with a dielectric constant of 80 for water. These corrections have been performed for intermediate as well transition state species. The table \ref{tab_SI:solv} in the Appendix provides the solvation energy correction for all the species considered in this study. The free energies used for all Figures in this chapter have been corrected for solvation unless stated otherwise. Vibrational and solvation corrections have been calculated for initial, final and transition states including the participating 2 explicit water molecules in order to plot the reaction free energy barriers where applicable.

\medskip

Vibrational analysis is also essential to confirm whether the structure obtained as a result of a geometry optimization is a minima (intermediate state (IS)) or a first-order saddle point (transition state (TS)). This can be concluded by diagonalizing the Hessian matrix whose eigen values give the force constants for all vibrational modes of the molecule. All positive eigen values confirm an IS whereas a single negative eigen value along the reaction coordinate signifies a TS. This check was done for all intermediates as well as transition state species obtained using CI-NEB calculations.

\medskip

All proton and electron transfers were assumed to be coupled (CPET) and the computational hydrogen electrode (CHE) model was used to obtain free energy change for an elementary step involving CPET \cite{Peterson2010}. The CHE model takes advantage of the equivalence of free energy of an electron and proton pair and hydrogen gas at standard conditions (equation (\ref{eq:CHE})). 0 V is defined vs RHE at which the reaction (\ref{eq:H2}) is at equilibrium (at all pH, all temperatures and $H_2(g)$ pressure of 101325 Pa). The $\Delta G$ as a function of electrode potential $U$ for a CPET step is then given by an expression such as (\ref{eq:CHE1}).

\begin{align}
\ce{H^{+}_{bulk} + e^{-}_{catalyst}  &<=> H2(g)}
\label{eq:H2}
\end{align}
\begin{equation}
\mu(H^+) + \mu(e^-) = \frac{1}{2}\mu(H_{2(g)}) - eU
\label{eq:CHE}
\end{equation}
\begin{equation*}
*X + H^+_{(aq)} + e^- \rightarrow *XH
\end{equation*}
\begin{equation}
\Delta G(U) = G_{*XH} - G_{*X} - (\frac{1}{2}G_{H_2} - eU)
\label{eq:CHE1}
\end{equation}

\medskip

Analogous to the CHE model for calculation of adsorption free energies, the CI-NEB calculations have been performed assuming equilibrium between the bulk protons and surface adsorbed H (reaction (\ref{eq:Hsurf})). This approach proposed recently by \citeauthor{AKHADE2017} \cite{AKHADE2017} has the advantage of using the bulk electrolyte as a reference state against which the reaction barriers are estimated instead of an arbitrary chemical potential of protons in a simulation cell. Also, the initial state of $*$\ce{H} is a stable local minima on the potential energy surface leading to a well defined reaction path. The activation barrier calculated is thus at the potential $U^o$ at which reaction (\ref{eq:Hsurf}) is at equilibrium, which in our case is -0.435 V vs. RHE as can be inferred from Figure \ref{fgr:2e}. For the reaction steps considered in this study, the change in work function of catalyst surface between the reactant, transition and final state can be considered negligible \cite{AKHADE2017}. 

\begin{align}
\ce{H^{+}_{bulk} + e^{-}_{catalyst} + $*$  &<=> $*$H}
\label{eq:Hsurf}
\end{align}

\medskip

Since the initial states of all reaction steps consist of a surface bound $*$\ce{H} (\ce{H^{$\delta -$}}), reactions requiring a \ce{H^{$\delta +$}} have been modeled using two additional water molecules close to the surface. The $*$\ce{H} hops through the two water molecules via a Grotthuss type mechanism forming \ce{H5O2^+} to eventually interact with the other reacting species such as another $*$\ce{H} or a \ce{CO2} close to the surface. Additional solvation might affect the energy of the reacting species which is why the energies of the initial, transition and final states have been corrected using implicit solvation model as described previously. The structure of the transition states located using this approach were checked to insure that the saddle points correspond to the formation of the respective final states (FS) and not for oxidation of $*$\ce{H} by water. This check is important for steps where the $*$\ce{H} first gets solvated by water before participating in the reaction of interest such as in the case of $*$\ce{COOH} and \ce{H2} via Heyrovsky step implying that the saddle point can be reached without forming $*$\ce{H} initially.

\medskip

Adsorbate-adsorbate interactions have been calculated on a 2x3x6 Ag (110) surface with the initial configuration of all the species present on their most favorable surface sites respectively (short-bridge for $*$\ce{H}, 2 on-top sites across long-bridge for $*$\ce{COOH} with the C and one of the O atoms bound to the surface, and 2 on-top sites across the short-bridge for $*$\ce{OCHO} with both the Os bound to the surface). This is followed by geometrical optimization with the same convergence criteria as described previously. Coverage value of a surface species corresponds to the fraction of the number of sites occupied and the total number of surface atoms. For e.g.: one $*$\ce{H} on a short-bridge site has a coverage of $\frac{1}{6}$ whereas one $*$\ce{OCHO} or \ce{COOH} has a coverage of $\frac{1}{3}$.

\medskip

The experimental methods used for the surface-enhanced Raman spectroscopy (SERS) results can be found in the Appendix in section \ref{sec:raman_exp}.

\section{Results and discussion}

\subsection{Mechanistic differences in e\ce{CO2}R pathways}

The lowest unoccupied molecular orbital (LUMO) of a bent \ce{CO2} is highly localized at the C, whereas the highest occupied molecular orbital (HOMO) is highly localized at the O, making them strongly susceptible to interactions with nucleophiles and electrophiles respectively \cite{Appel2013}. We use Bader charge analysis \cite{Bader1994,Tang2009} to quantify this susceptibility and to chart the reaction path for the first CPET step for \ce{CO2} reduction, as depicted in Figure \ref{fgr:Bader}. In order for the \ce{C-H} bond of $*$\ce{OCHO} to form, $*$\ce{H} with a partial negative charge ($\delta^{-}$) acts as a nucleophile for the \ce{C^{$\delta^{+}$}} of \ce{CO2} (top panel in Figure \ref{fgr:Bader}). Both Volmer-Tafel and Volmer-Heyrovsky mechanisms for the formation of \ce{H2} also share the first CPET Volmer step of $*$\ce{H^{$\delta^{-}$}} formation (bottom panel in Figure \ref{fgr:Bader}). This implies that the formation of $*$\ce{COOH} competes with the Volmer step which is in-turn a prerequisite for the formation of $*$\ce{OCHO}. Following the Volmer step, the $*$\ce{H} can either participate in a Tafel or Heyrovsky step to form \ce{H2}, or react with the \ce{CO2} to form $*$\ce{OCHO}. According to our analysis, there are therefore two reaction bifurcations before and after formation of $*$\ce{H} that generally control the selectivity for e\ce{CO2}R. An analogous finding has been recently reported for e\ce{CO2}R on Cu(100) surfaces \cite{Goddard2016} and as well as for the selectivity of e\ce{CO2}R on metalloporphyrins \cite{Koper2018}.  

\begin{figure}
\centering 
\includegraphics[width=0.95\textwidth]{chapter_angewandte/figures/competition.png}
\caption{A cartoon depicting the first reduction step for e\ce{CO2}R pathways to form $*$\ce{COOH} (\ce{CO} pathway) and $*$\ce{OCHO} (\ce{HCOO^-} pathway) on an Ag(110) surface (violet box above) along with the two \ce{H2} formation pathways: Volmer-Tafel and Volmer-Heyrovsky (peach box below). The excess partial charges for the relevant chemical species is mentioned in blue for a excess positive charge and in red for excess negative charge.}
\label{fgr:Bader}
\end{figure}

\medskip

This approach can be further extended to the formation of higher CPET products from e\ce{CO2}R such as methanol (\ce{CH3OH}) and methane (\ce{CH4}). Figure \ref{fig:reactions} in the Appendix shows the proposed general reaction network for \ce{CO2} electro-reduction for formation of C1 species. C2 and higher species have been ignored in this network since the focus of this study is Ag electrodes, which show high selectivity for C1 products and hydrogen, and thus no appreciable amount of C2 products are expected to form \cite{Hori2008,Hatsukade2014}. This scheme has been generated from the reaction networks reported in literature \cite{Kortlever2015,Hatsukade2014} in addition to a general chemical intuition. Figures \ref{fig:6e} and \ref{fig:8e} show the free energy plots for the formation of \ce{CH3OH} and \ce{CH4} in accordance with the reaction scheme of Figure \ref{fig:reactions}. The thermodynamically most feasible routes for the formation of \ce{CH3OH} and \ce{CH4} go via $*$\ce{COOH} followed by the $*$\ce{CHO} species, as becomes clear from the $U_L$ for the various reaction pathways shown in Figures \ref{fig:6e} and \ref{fig:8e}. Extending the Bader charge analysis to these pathways, it is expected that the formation of $*$\ce{CHO} from $*$\ce{CO} forms through a surface bound $*$\ce{H} interacting with the \ce{C^{$\delta$+}}. $\theta_{*H}$ is therefore expected to play an important role in the formation of higher electron reduction products of e\ce{CO2}R and this analysis can potentially be useful for studying catalysts such as Cu where these reaction steps become prominent. Interestingly, very recent experimental findings for e\ce{CO2}R on Cu catalysts draw similar conclusions and indicate an important role of $*$\ce{H} in the formation of \ce{CH4} \cite{Yogi2018,Asthagiri2013}. 

\begin{figure}
\centering
\includegraphics[width=0.8\textwidth]{chapter_angewandte/figures/SI9_free_energy_plot_110_CH3OH.pdf}
\caption{Free energy diagram showing pathways for formation of \ce{CH3OH(aq)} for Ag(110) surface. The limiting potentials ($U_L$) in the legend for all the pathways are vs. RHE.}
\label{fig:6e}
\end{figure}

\medskip

\begin{figure}
\centering
\includegraphics[width=0.8\textwidth]{chapter_angewandte/figures/SI10_free_energy_plot_110_CH4.pdf}
\caption{Free energy diagram showing pathways for formation of \ce{CH4(g)} for Ag(110) surface. The limiting potentials ($U_L$) in the legend for all the pathways are vs. RHE.}
\label{fig:8e}
\end{figure}

\medskip

\subsection{Quantification of reaction barriers}

Climbing image nudged elastic band (CI-NEB) calculations were performed to estimate the height of the activation barriers based on the reaction path analysis presented in Figure \ref{fgr:Bader}. Figures \ref{fgr:barrier_COOH} to \ref{fig:barrier_H2_taf} show the reaction paths and activation barriers as calculated using CI-NEB. The energy values in these plots represent $E_{DFT}$ and are not corrected for vibrational entropy, enthalpy, zero point energy and solvation. The data points in blue represent the system images along the potential energy surface in the direction of the transition state. The Bader charge analysis tables at the bottom of each plot give the excess negative or positive charge on the atoms. Figure \ref{fig:barrier_corrs} in the Appendix shows the influence of the various corrections to the activation barriers calculated using CI-NEB calculations. 

\medskip

As can be seen in Figure \ref{fgr:barrier}, the activation barriers after correction for solvation for formation of $*$\ce{COOH} and \ce{H2} via Heyrovsky and Tafel steps are 0.93 eV, 0.79 eV and 0.77 eV respectively, with \ce{H2} as the most thermodynamically favorable product. Interestingly however, there is no kinetic barrier for the formation of $*$\ce{OCHO} involving the direct nucleophilic attack by $*$\ce{H}. The high solvation energy of the transition state relative to the initial state for $*$\ce{OCHO} (Table \ref{tab_SI:solv}, Figure \ref{fig:barrier_corrs}) plays an important role in diminishing the activation barrier for its formation. This analysis highlights the importance of the consideration of solvation in theoretical mechanistic studies for e\ce{CO2}R; a conclusion which is in agreement with what has been shown for other (electro)catalytic systems \cite{Sautet2014,Sautet2013,Goddard2010}.

\medskip

The presence of cationic and anionic species in the electrolyte may also influence the binding energies and activation barriers for the formation of certain reaction intermediates and there are ongoing efforts to include these effects in computational studies pertaining to e\ce{CO2}R \cite{Chen2016,Resasco2017}. Based on the results in Figure \ref{fgr:barrier}, we show that there is a significant energy barrier for the formation of the \ce{CO} pathway intermediate $*$\ce{COOH}, whereas $*$\ce{OCHO} is expected to form readily on the Ag(110) surface in the presence of $*$\ce{H}.

\begin{figure}[hp]
\centering
\includegraphics[scale=0.55]{chapter_angewandte/figures/SI3_1_CO2-COOH-Heyrovsky.png}
\includegraphics[scale=0.7]{chapter_angewandte/figures/SI3_2_bader_tables_COOH.png}
\caption{(top) Activation barrier for the formation of $*$\ce{COOH} along with the geometrical configuration of the IS: initial state, TS: transition state and FS: final state. (bottom) Excess Bader charges on reacting atoms.}
\label{fgr:barrier_COOH}
\end{figure}

\medskip

\begin{figure}[p]
\centering
\includegraphics[scale=0.55]{chapter_angewandte/figures/SI4_1_CO2-OCHO-climb_img2.png}
\includegraphics[scale=0.7]{chapter_angewandte/figures/SI4_2_bader_tables_OCHO.png}
\caption{(top) Activation barrier for the formation of $*$\ce{OCHO} along with the geometrical configuration of the IS: initial state, TS: transition state and FS: final state. Images $I_1$ and $I_2$ have been shown for the sake of clarity of the reaction path. (bottom) Excess Bader charges on reacting atoms.}
\label{fgr:barrier_OCHO}
\end{figure}

\medskip

\begin{figure}[p]
\centering
\includegraphics[scale=0.55]{chapter_angewandte/figures/SI5_1_H_H2_Heyrovsky2.png}
\includegraphics[scale=0.7]{chapter_angewandte/figures/SI5_2_bader_tables_H2_Hey.png}
\caption{(top) Activation barrier for the formation of \ce{H2} via a Heyrovsky mechanism along with the geometrical configuration of the IS: initial state, TS: transition state and FS: final state. (bottom) Excess Bader charges on reacting atoms.}
\label{fig:barrier_H2_hey}
\end{figure}

\medskip

\begin{figure}[p]
\centering
\includegraphics[scale=0.55]{chapter_angewandte/figures/SI6_1_2H-H2-Tafel.png}
\includegraphics[scale=0.7]{chapter_angewandte/figures/SI6_2_bader_tables_H2_Tafel.png}
\caption{(top) Activation barrier for the formation of \ce{H2} via a Tafel mechanism along with the geometrical configuration of the IS: initial state, TS: transition state and FS: final state. (bottom) Excess Bader charges on reacting atoms.}
\label{fig:barrier_H2_taf}
\end{figure}

\medskip

\begin{figure}[p]
\centering
\includegraphics[scale=1]{chapter_angewandte/figures/barriers_allcorr.pdf}
\caption{Free energy of activation for Ag(110) at a reference potential $U^o$=-0.435 V vs. RHE for the equilibrium between surface bound $*$\ce{H} and the bulk proton and electron pair.}
\label{fgr:barrier}
\end{figure}

\newpage

\begin{figure}[h]
\centering
\includegraphics[scale=0.55]{chapter_angewandte/figures/1750_1200cm1.png}
\caption{SERS spectra for e\ce{CO2}R on polycrystalline Ag in 0.05 M \ce{Li2B4O7} saturated with \ce{CO2} with a bulk pH of 6.1. The spectral region shows the O-bound bidentate species and the shift towards lower frequencies as we apply more cathodic potentials. The blue arrows indicate the formation of a new band at 1298 cm$^{-1}$ related to the merging and shift of the bidentate bands. All potentials are given vs. RHE.}
\label{fgr:raman1}
\end{figure}

In order to validate our theoretical findings, preliminary \textit{in-situ} electrochemical SERS measurements were performed to probe polycrystalline Ag catalyst surface during e\ce{CO2}R in 0.05 M \ce{Li2B4O7} \cite{Isis2017,Jan2013} saturated with \ce{CO2}, with a bulk pH of 6.1 (see section \ref{sec:raman_exp} for details). We observe a double-band shape at 1436 and 1469 cm$^{-1}$ (Figure \ref{fgr:raman1}) corresponding to an O-bound bidentate intermediate on the Ag surface, which we believe to be the $*$\ce{OCHO} species based on similar evidence obtained in literature for carboxylate species on silver hydrosols \cite{Kai1989}. The interactions of O-bound species with the Ag surface appear at relatively low overpotentials (-0.52 V vs RHE), in agreement with the obtained theoretical results. We also observe a correlation in the appearance of a $\delta$\ce{C-H} vibration band at 1298 cm$^{-1}$ \cite{vibspec} with the bidentate signal merging into a broader band at more cathodic potentials of -1.12 V vs RHE as can be seen in Figure \ref{fgr:raman1}. The same correlation is observed using a lithium borate buffer solution with a bulk pH of 6.9 albeit at higher overpotentials (Figure \ref{fig:raman_10}). These experiments suggest that pH plays an important role in e\ce{CO2}R at lower overpotentials due to its implications on the formation of $*$\ce{H} on the Ag electrode.

\medskip

\subsection{Lateral adsorbate interactions}

\begin{figure}[p]
\centering
\includegraphics[scale=0.7]{chapter_angewandte/figures/G_cover_arrows.pdf}
\caption{Free energy diagram for formation of \ce{CO(g)}, \ce{HCOO^-(aq)} and \ce{H2(g)} on Ag(110) surface at 0 V vs. RHE. The adsorption energies shown in dark red, blue and green are in the presence of $*$\ce{OCHO} ($\theta$=1/3), whereas the energies in light red, blue and green are without $*$\ce{OCHO}. The upward arrows denote the change in free energy of the respective intermediates due to the presence of $*$\ce{OCHO} on the surface. Formation of $*$\ce{OCHO} has been shown to occur following the formation of $*$\ce{H} as per the proposed mechanism and is not an electron transfer step.  Limiting potentials ($U_L$) are given vs. RHE.}
\label{fgr:cover1}
\end{figure}

\medskip

Adsorbate-adsorbate interactions are known to play an important role in determining the energetics of surface reactions including e\ce{CO2}R \cite{Grabow2010,Jaramillo2018}. Considering the high likelihood of the presence of $*$\ce{OCHO} species on the Ag catalyst surface, we investigate its influence on the adsorption energies of reaction intermediates involved in the 2-electron reduction processes during e\ce{CO2}R. The free energy diagram for Ag(110) in Figure \ref{fgr:cover1} shows that the presence of $*$\ce{OCHO} weakens the $*$\ce{H} binding significantly (green pathway in Figure \ref{fgr:cover1} and Figure \ref{fig:coverage_H}) whereas the effect of its presence on the binding energies of the $*$\ce{COOH}, *\ce{OCHO} and $*$\ce{CO} species is relatively smaller. As a result, the $U_L$ for the formation of \ce{H2} and \ce{HCOO^-} (both proceeding via the Volmer step), become much less favorable and comparable to the $U_L$ for the formation of \ce{CO}.

\medskip

Figure \ref{fig:coverage_COOH} shows the effect of the coverage of $*$\ce{H} ($\theta_{*H}$) on the binding energy of $*$\ce{COOH} for Ag(110). It is clear that as $\theta_{*H}$ increases, the binding of $*$\ce{COOH} on the catalyst surface becomes increasingly thermodynamically unfavorable. The presence of $*$\ce{OCHO} therefore has two important consequences for the selectivity of Ag e\ce{CO2}R catalysts: firstly, $*$\ce{OCHO} weakens the binding strength of $*$\ce{H} with the catalyst surface, bringing the \ce{CO} formation pathway to a level-playing field thermodynamically with the \ce{H2} and \ce{HCOO^-} pathways. Secondly, the lower $\theta_{*H}$ as a consequence of the weaker $*$\ce{H} binding enables the formation of $*$\ce{COOH}, thereby improving the selectivity of the catalyst towards \ce{CO}. The influence of lateral adsorbate interaction of $*$\ce{OCHO} on the binding strength of $*$\ce{H} diminishes as the activity of the catalytic surface reduces (see Figure \ref{fig:fig13}). The lower binding strength of $*$\ce{COOH} in addition to the negligible effect on the binding strength of $*$\ce{H} in the presence of $*$\ce{OCHO} for Ag(111) (Figure \ref{fig:G_cover_111}) is in line with the experimental observation that close-packed surfaces such as Ag(111) have drastically lower activity for e\ce{CO2}R to CO relative to Ag(110) \cite{Hoshi1997}. This analysis highlights the importance of the consideration of lateral adsorbate-adsorbate interactions to bridge the discrepancy between theoretical predictions and the experimental observations for Ag as well as other e\ce{CO2}R catalysts.

\medskip

\section{Conclusions and Discussion}
 
From our theoretical analysis, we come to the conclusion that the O-bound formate precursor species $*$\ce{OCHO}, which is typically considered irrelevant for the \ce{CO} producing catalyst Ag, should not only be present on the surface at low overpotentials during e\ce{CO2}R, but is also likely playing an active role in promoting the selectivity of Ag towards CO production. In addition, the results strongly indicate that factors such as $\theta_{*H}$ and solvation by surrounding water molecules will play an important role in controlling selectivity between the various e\ce{CO2}R products. Calculation of activation barriers at constant potential and adjusted for activities of reactant and electrolyte species in order to simulate the conditions during electrocatalysis remains a challenge and there is a need for further development of computational methodologies for this purpose. Our approach demonstrates a constructive interplay between theory and experiments to advance the understanding of a complex system of high practical significance. The results highlight the need to study the catalyst surface in its operational steady state, both theoretically and experimentally, to understand the synergistic and competitive effects between the reaction species that ultimately result in the observed performance.

\references{chapter_angewandte/bib_angewandte}

