\chapter{The competition between two-electron reduction products}
\label{angewandte_appendix}

%% The following annotation is customary for chapter which have already been
%% published as a paper.
\blfootnote{This appendix has been published as supplementary information in Angew. Chem. Int. Ed. \textbf{58}, 1345 (2019) \cite{Divya_angewandte}.

Computational research work led by D. Bohra and supervised by G. Li and E. A. Pidko; Experimental research work led by I. Ledezma-Yanez and supervised by W. de Jong; W. A. Smith supervised the project as the principal investigator.}

\section{Experimental Methods: SERS}\label{sec:raman_exp}

\begin{figure}[h]
\centering
\includegraphics[scale=0.4]{chapter_angewandte/figures/SI15_CV_Ar.png}
\caption{Cyclic voltammograms recorded during the Ag roughening in KCl 0.1 M}
\label{fig:raman_1_SI}
\end{figure}

\medskip

All experiments were carried out in a three-electrode cell configuration: a flame-annealed silver wire served as counter electrode, an eDAQ leak-less Ag/AgCl electrode was used as reference electrode, and a home-made silver electrode (99.999\% pure Ag) was designed in a L-shape as a working electrode surface, orthogonal to the laser probe for Raman spectra acquisition. The electrolyte solutions were freshly prepared using Millipore MilliQ water (resistivity > 18.2 M$\Omega$.cm), and the reactants used are listed as lithium tetraborate (99.995\% trace metals basis, Sigma-Aldrich), boric acid (99.9999 Suprapur, Sigma-Aldrich) and KCl (anhydrous, beads, trace metals basis, Sigma-Aldrich). Argon (5.5) and carbon dioxide (6.0) gases were purchased from Linde. 

\begin{figure}[ht]
\centering
\includegraphics[scale=0.4]{chapter_angewandte/figures/SI16_CV_Ar2.png}
\caption{Cyclic voltammograms recorded in \ce{Li2B4O7} 0.05 M under argon atmosphere (5 scans).}
\label{fig:raman_2}
\end{figure}

\medskip

Prior to experiments, the glassware was cleaned by boiling it in a 1:1 mixture of concentrated sulfuric and nitric acids, rinsed thoroughly, and boiled with Millipore MilliQ water five times for each time we changed electrolyte. It was kept in the oven at 90 $^{\circ}$C when not in use. The working electrode was polished mechanically to mirror finish with alumina of three different meshes, in descending order of grain size: 1 $\mu$m, 0.5 $\mu$m and 0.03 $\mu$m. A 10:90 ethanol/water mixture was prepared to clean the electrode from alumina traces by applying an ultrasonic bath for 15 minutes. After this treatment, the electrode was rinsed with abundant ultrapure water and used as is for recording cyclic voltammograms. For surface-enhanced Raman probing, a roughening procedure was used to generate a plasmonic surface on the working electrode: 26 consecutive oxidation/reduction cycles with a 1.5 s step at 0.2 V vs. Ag/AgCl per each cycle, in a 0.1 M KCl solution. The voltammograms recorded are shown in Figure \ref{fig:raman_1_SI}. 
Cyclic voltammograms and potential control were recorded using a BioLogic SP-200 potentiostat/galvanostat, whereas the surface-enhanced Raman measurements were collected using a RAMANRXN2 multichannel-532 from Kaiser Optical Systems, with a 532 nm Invictus\texttrademark VIS laser (100 mW), with a spectral resolution of 5 cm$^{-1}$. A total of 20 interferograms were collected in an interval of 120 s per each measurement. 

\medskip

Figure \ref{fig:raman_2} shows the blank cyclic voltammograms obtained from the silver electrode in 0.05 M \ce{Li2B4O7} solution purged with argon (pH=9.5). A total of five scans were recorded in a cathodic working window of 2 V, at a scan rate of 100 mV/s. A redox couple with a reduction peak ca. -0.66 V vs. Ag/AgCl is shown in the insert of Figure \ref{fig:raman_2}. This feature corresponds to the formation of LiOH on the electrode surface due to the high pH of the solution. Still, no significant shift in pH, current or shape is observed after the five scans, suggesting that the formation of LiOH does not affect the active sites for hydrogen evolution nor the interfacial pH in an irreversible way.

\begin{figure}[ht!]
\centering
\includegraphics[scale=0.4]{chapter_angewandte/figures/SI17_CV_CO2.png}
\caption{Cyclic voltammograms recorded in \ce{Li2B4O7} 0.05 M under \ce{CO2} atmosphere (continuous bubbling; 3 scans).}
\label{fig:raman_3}
\end{figure}

\medskip

\begin{figure}[ht!]
\centering
\includegraphics[scale=0.4]{chapter_angewandte/figures/SI18_CVs_stack_RHE.png}
\caption{Cyclic voltammograms recorded in \ce{Li2B4O7} 0.05 M under Ar and \ce{CO2} atmospheres respectively (same data as in Figures \ref{fig:raman_2} and \ref{fig:raman_3}) with potentials corrected for pH and reported vs RHE.}
\label{fig:raman_rhe}
\end{figure}

\medskip

Figure \ref{fig:raman_3} contains the cyclic voltammograms recorded on a silver electrode in 0.05 M \ce{Li2B4O7} solution, saturated with carbon dioxide under continuous \ce{CO2} flux (pH=6.1; carbonic acid regime). The scan rate used was 20 mV/s, in a 2 V working window with a pH correction of 180 mV/Dec pH when compared to the original electrolyte (pH=9.5). The voltammograms shift towards cathodic potentials after each consecutive cycle, evidencing the pH change on the electrode interface due to the water decomposition into hydrogen and hydroxyl groups. This translates into a more alkaline interface; 6.1< pH <9.5, since the LiOH redox couple is not observed as in Figure \ref{fig:raman_2}. The insert in Figure \ref{fig:raman_3} shows a reduction peak with an onset potential at -0.828 V vs. Ag/AgCl (ca. -1.2 V vs RHE), attributed to \ce{CO2} reduction in the first scan. The second and third scans are featureless. Although the current is not normalized per surface area, the geometrical area of the electrode is the same for the voltammograms presented in Figures \ref{fig:raman_2} and \ref{fig:raman_3}, suggesting that the current in presence of dissolved \ce{CO2} is larger when compared to that obtained under argon atmosphere, provided the correction for applied potential. The pH increase translates into an increase of the free energy, decreasing with this the thermodynamic driving force of the reaction. Figure \ref{fig:raman_rhe} shows the cyclic voltammetries from Figures \ref{fig:raman_2} and \ref{fig:raman_3} with the measured potentials corrected for pH.

\newpage

\section{Supporting Results}

\subsection{SERS measurements}

This section provides spectroscopic data collected from the electrochemical \ce{CO2} reduction performed on a silver electrode immersed in a 0.05 M \ce{Li2B4O7} solution, saturated with \ce{CO2}. The electrolyte choice plays an important role in the stabilization of intermediates, given the fact that alkali metals modify the surrounding water networks by affecting the entropy of solvation at the electrode interface. We choose an electrolyte containing lithium ions (high electronic affinity) to facilitate the proton transfer to the surface and the formation of dipoles necessary to stabilize the short-lived intermediates during the measurements, since \ce{CO2}ER proceeds in a low proton availability regime (pH>3).

\begin{figure}[h]
\centering
\includegraphics[scale=0.5]{chapter_angewandte/figures/SI19_Transmission_Ar_CO2.png}
\caption{Raman measurements recorded in 0.05 M borate buffer solutions under Ar atmosphere and \ce{CO2} saturation, respectively.}
\label{fig:raman_4}
\end{figure}

\medskip

\begin{figure}[h]
\centering
\includegraphics[scale=0.5]{chapter_angewandte/figures/SI20_Transmission_CO2_CO2.png}
\caption{Raman measurements recorded in 0.05 M \ce{Li2B4O7} and 0.05 M borate buffer solutions saturated with \ce{CO2}; pH 6.1.}
\label{fig:raman_5}
\end{figure}

\medskip

Prior the electrocatalysis, transmission Raman spectra from the solutions were collected to identify the formation of the carbonated species in the electrolyte, compared with the solutions saturated with argon. Figure \ref{fig:raman_4}, bottom panel, shows the transmission spectra collected from 0.05 M borate buffer solutions, with an initial pH=9.0. The solution was saturated with argon, with no pH change, as expected. The water bending can be observed ca. 1630 cm$^{-1}$ and the bands ca. 850, 750 and 420 cm$^{-1}$ are attributed to buffer solution. The spectra corresponding to the borate buffer saturated with \ce{CO2} is shown in the top panel of Figure \ref{fig:raman_4}. The pH decreased to 6.1 after one hour of constant \ce{CO2} bubbling, confirming the acidification of the solution (formation of carbonic acid as solvated \ce{CO2}). Raman shift frequencies corresponding to the presence of dissolved \ce{CO2} are identified as 1382 cm$^{-1}$ from $\nu$ \ce{CO2}, 1276 cm$^{-1}$ from $\delta$ C-OH, and 1019 cm$^{-1}$ as $\nu$ C-OH from \ce{HCO3-} \cite{Wolfram2008}. Figure \ref{fig:raman_5} shows the comparison of the spectra collected for 0.05 M \ce{Li2B4O7} and 0.05 M borate buffer solutions saturated with \ce{CO2}. The spectra reveal the same bands for both solutions at the final measured pH=6.1, suggesting that the equilibrium between carbonate/bicarbonate in lithium tetraborate and boric acid behaves in a similar way.

\begin{figure}[h]
\centering
\includegraphics[scale=0.9]{chapter_angewandte/figures/SI21_raman_full_spectra.png}
\caption{SERS measurements recorded as a function of applied potential in 0.05 M \ce{Li2B4O7} saturated with \ce{CO2} at bulk pH 6.1.}
\label{fig:raman_6}
\end{figure}

\medskip

\begin{figure}[h]
\centering
\includegraphics[scale=1]{chapter_angewandte/figures/SI22_1100_400_cm1.png}
\caption{Zoomed-in SERS measurements recorded as function of applied potential in 0.05 M \ce{Li2B4O7} saturated with \ce{CO2} at bulk pH 6.1. Raman shift region: 1100 - 400 cm$^{-1}$. }
\label{fig:raman_7}
\end{figure}
    
\medskip

We present the full spectra collected for in-situ SERS for \ce{CO2}ER on polycrystalline Ag in Figure \ref{fig:raman_6}. The most prominent bands are shown in the region 3600 - 3000 cm$^{-1}$, corresponding to the water, followed by \ce{C-H} interactions between 3000 - 2650 cm$^{-1}$. Due to its complexity, the region between 1750 - 200 cm$^{-1}$ is presented in three different sections: the 1750 - 1300 cm$^{-1}$ region corresponds to Figure \ref{fgr:raman1} in the chapter \ref{angewandte}, and the 1100 - 400 cm$^{-1}$ and the 300 - 200 cm$^{-1}$ regions are shown with our band assignments in Figures \ref{fig:raman_7} and \ref{fig:raman_8}, respectively. For the figures used for band assignment, the electrolyte was subtracted using a SERS spectrum taken at open circuit potential. 

\medskip

From Figure \ref{fig:raman_6} the most prominent bands correspond to water stretching and water bending, observed at 3000 - 3750 cm$^{-1}$ and 1630 cm$^{-1}$ respectively. In this raw full spectra it is shown how strong the baseline distortion is from the water deformation, although it may have a more complex origin related to an artifact that we cannot discard. Nonetheless, when we approach more cathodic potentials (-1.12 V), we observe the formation of other species (See Figure \ref{fig:raman_7}, region 600 - 700 cm$^{-1}$) and the shifting of bands that concertedly happen with an ease or relaxation of these baseline deformations. We believe that these phenomena are related to H-bond breaking and making, which becomes quite slow in presence of electrolytes with pH higher than 3, when the hydrogen evolution depends on protons from water and the stabilization capacity of the catalyst, aided by cation solvation processes.

\medskip

We probed polycrystalline Ag catalyst surface during \ce{CO2}ER using in-situ electrochemical SERS measurements in 0.05 M \ce{Li2B4O7} \cite{Isis2017,Jan2013} saturated with \ce{CO2}, with a bulk pH of 6.1. We assign the bands at 1436 and 1469 cm$^{-1}$ to an O-bound bidentate intermediate on the Ag surface, which we believe corresponds to $*$\ce{OCHO} (Figure \ref{fgr:raman1} in chapter \ref{angewandte}). Evidence of an O-bound bidentate configuration with a similar double-band shape and spectral region has been discussed previously for carboxylate species on silver hydrosols studied with SERS \cite{Kai1989}. Factors such as the applied electric field, interaction with the other surface species, the protonated carbon atom as opposed to the carboxylate species reported by \citeauthor{Kai1989} \cite{Kai1989}, will affect the frequency at which the bidentate double-band appears. The anti-Stark effect can explain the 1469 cm$^{-1}$ band shift towards slightly negative frequencies, with the so-called resonance transfer among species containing \ce{C=O} groups being another potential cause for this shift \cite{Wang1980,Mahan1978}. As we approach more cathodic potentials (-1.12 V), the bidentate signal merges into a broader band and a new band forms at 1298 cm$^{-1}$, corresponding to a $\delta$\ce{C-H} vibration \cite{vibspec}.

\medskip

In the case of the formation of a monodentate configuration, a band should appear at 1567 cm$^{-1}$ (denoted with a red dotted line in Figure \ref{fgr:raman1} in chapter \ref{angewandte}). However, the baseline deformation is strong and the observation is inconclusive. The species $I_1$ in Figure \ref{fgr:barrier_OCHO} shows the configuration of a monodentate O-bound species formed along the reaction path in the CI-NEB calculation leading to the more stable bidentate intermediate $*$\ce{OCHO}. In our geometrical optimization calculations, we find that the species $I_1$, or $*$\ce{HCOO}, can be metastable in the sense that the potential energy surface is flat in the vicinity of this configuration. We suspect that at high overpotentials, the monodentate species $*$\ce{HCOO} can be stabilized to a greater extent due to a stronger \ce{Ag-H} interaction and solvation by tightly bound interfacial water molecules. It is worth noting that the solvation energy of $*$\ce{HCOO} is significantly higher than for the $*$\ce{OCHO} species in the absence of electric field (Table S1). According to the theoretical calculations, there is a prerequisite Volmer step in order for the O-bound $*$\ce{OCHO} intermediate to form. The baseline distortions observed experimentally in the water deformation region may be linked to the occurrence of this Volmer step in addition to the effect of polarized species on the dipole moment of the interfacial water molecules.

\medskip

Zooming in into the 1100 - 400 cm$^{-1}$ region of the full spectra shown in Figure \ref{fig:raman_6}, (Figure \ref{fig:raman_7}), we identify several vibrational modes from species related to the \ce{CO2}ER, confirming that the catalysis is taking place. The prominent features are related to \ce{O-C} and \ce{O-C-O} species, and the band assignment was made based on previous reports for Raman spectroscopy analysis performed for liquid samples and SERS analysis on silver substrates \cite{Wolfram2008,Kai1989,vibspec}.

\begin{figure}[ht!]
\centering
\includegraphics[scale=1]{chapter_angewandte/figures/SI23_300_200cm1.png}
\caption{Zoomed-in SERS measurements recorded as function of applied potential in 0.05 M \ce{Li2B4O7} saturated with \ce{CO2} at bulk pH 6.1. Raman shift region: 300 - 200 cm$^{-1}$.}
\label{fig:raman_8}
\end{figure}

\medskip

\begin{figure}[ht!]
\centering
\includegraphics[scale=0.5]{chapter_angewandte/figures/SI24_2200_1700cm_1.png}
\caption{Zoomed-in SERS measurements recorded as function of time in 0.05 M \ce{Li2B4O7} saturated with \ce{CO2} at bulk pH 6.1. At the applied potential of -2.02 V we observe the formation of CO at 2021 cm$^{-1}$.}
\label{fig:raman_9}
\end{figure}

\medskip

Figure \ref{fig:raman_8} shows the 300 - 200 cm$^{-1}$ region of the SERS spectra shown in Figure \ref{fig:raman_6}. There are strong bands present at 243.5 and 251 cm$^{-1}$ (Figure \ref{fig:raman_8}), that can be attributed to \ce{Ag-O} interactions with the carbonated species formed from the \ce{CO2}ER. The absence of $\nu$\ce{Ag-O} bands at 330, 470 and 860 cm$^{-1}$ reported previously \cite{Iwasaki1988} rule out the formation of surface oxides and hydroxides, reinforcing our band assignment of the O-bound species. 

\medskip

From the spectra in Figure \ref{fig:raman_9}, we observe no \ce{CO} formation until -2.02 V vs. RHE, presented as a function of time with a fixed applied potential.

\begin{figure}[ht!]
\centering
\includegraphics[scale=0.8]{chapter_angewandte/figures/SI25_1750_300cm1.png}
\caption{Zoomed-in SERS measurements recorded as function of time in 0.05 M borate buffer saturated with \ce{CO2} at bulk pH 6.9. Raman shift region: 1750 - 300 cm$^{-1}$.}
\label{fig:raman_10}
\end{figure}

\medskip

SERS experiments for \ce{CO2}ER on polycrystalline silver were performed in lithium borate buffer solutions, pH 6.9, and are shown in Figure \ref{fig:raman_10}. The region corresponds to 1750 - 300 cm$^{-1}$ and shows the bands from the electrolyte solution as well as the water bending. At -2.03 V vs. RHE, we observe the formation of a double band: 1442 and 1421 cm$^{-1}$ attributed to a \ce{O-(CH)-O} vibration bonded to the Ag through both oxygen atoms, as can also bee seen in Figure \ref{fgr:raman1} in chapter \ref{angewandte}. We also observe the concerted formation of a band at 1298 cm$^{-1}$ corresponding to a \ce{C-H} deformation \cite{vibspec}, and a band at 1162 cm$^{-1}$ associated with the bidentate species. These experiments suggest that pH plays a role in \ce{CO2}ER at lower overpotentials due to its dependence on the hydride formation on the Ag electrode. The \ce{C-H} band as seen in Figure \ref{fgr:raman1} in chapter \ref{angewandte} for the 0.05 M \ce{Li2B4O7} buffer (pH 6.1) is also formed in presence of the borate buffer solution as shown in Figure \ref{fig:raman_10}, along with the formation of the bidentate species at high overpotentials, indicating a correlation in the formation of these species.

\medskip

Figure \ref{fig:raman_10} also shows a unique band at 1382 cm$^{-1}$ from -0.19 V vs.RHE which is attributed to $\nu$\ce{CO2} by \citeauthor{Wolfram2008} \cite{Wolfram2008}. This band is also observed in the transmission spectra of our \ce{CO2}-saturated electrolyte solution (see Figures \ref{fig:raman_4} and \ref{fig:raman_5}), and corresponds to one of the two $\nu$\ce{COO-} bands assigned by \citeauthor{Kai1989} \cite{Kai1989} for bidentate carboxylate. The appearance of bands at 1442 and 1421 cm$^{-1}$ at -2.06 V in Figure \ref{fig:raman_10} present a slight baseline deformation while the hydrogen evolution is occurring suggesting that, firstly, the band we assign as bidentate is independent from the 1382 cm$^{-1}$ band, hence the coexistence; and secondly, there is a sensitivity to hydrogen population on the electrode surface in order for the 1442 and 1421 cm$^{-1}$ species to form. Note that there is also a frequency shift of these bands when compared to the bidentate species assigned in Figure \ref{fgr:raman1} in chapter \ref{angewandte}, showing how slight changes in the measurement conditions (bulk pH in this case) may lead to frequency shifts. 

\newpage

\subsection{Computational results}

\begin{figure}[ht!]
\centering
\includegraphics[scale=0.55]{chapter_angewandte/figures/SI1_free_energy_plot_110_111_211_wH2.pdf}
\caption{Free energy plot showing \ce{CO(g)} and \ce{HCOO^-(aq)} formation pathways for (111), (211) and (110) Ag surfaces. The limiting potentials ($U_L$) are vs. RHE. The free energy values have been corrected for solvation (see Table \ref{tab_SI:solv}).}
\label{fig:111_211_110}
\end{figure}

\newpage

\begin{figure}
\centering
\includegraphics[width=0.9\textwidth]{chapter_angewandte/figures/SI8_reaction_scheme.pdf}
\caption{A schematic of the proposed general reaction network for e\ce{CO2}R. Network colored blue goes via $*$\ce{OCHO} intermediate, red goes via $*$\ce{COOH} intermediate and green pathway leads to the direct formation of $*$\ce{CO}+$*$\ce{OH} by breaking of the \ce{C-O} bond on protonation of the O atom. Colored molecules are product molecules from the respective pathways whereas black molecules are surface bound intermediates. Dotted arrows denote merging of one pathway with another.}
\label{fig:reactions}
\end{figure}

\newpage

\begin{figure}[p]
\centering
\includegraphics[scale=0.55]{chapter_angewandte/figures/SI7_barrier_corrs.png}
\caption{a) Activation barriers without any corrections (only $E^{DFT}$). b) Activation barriers corrected for only solvation ($E^{DFT}$ - $E_{solv}$). c) Activation barriers corrected for vibrational entropy, enthalpy, zero point energy as well a systematic DFT functional errors but not solvation or in other words the free energy of activation in vacuum ($E^{DFT}$ + $E^{ZPVE}$ + $\int_{T=0}^{T} C_{p}dT$ - $TS^{vib}$ + $E_{xc}$). d) Free energy of activation corrected for solvation ($E^{DFT}$ + $E^{ZPVE}$ + $\int_{T=0}^{T} C_{p}dT$ - $TS^{vib}$ + $E_{xc}$ - $E_{solv}$) same as Figure \ref{fgr:barrier} in chapter \ref{angewandte}.}
\label{fig:barrier_corrs}
\end{figure}

\medskip

Figure \ref{fig:barrier_corrs} shows the influence of the various corrections to the activation barriers calculated using CI-NEB calculations. The values of $U^o$ (the potential for which equation (\ref{eq:Hsurf}) is at equilibrium) is different for every plot in Figure \ref{fig:barrier_corrs} due the the different potentials needed to bind $*$\ce{H} exergonically depending on the level of corrections applied to $E^{DFT}$. Solvation correction has a significant influence on the activation barrier for formation of $*$\ce{OCHO} whereas the free energy corrections have a significant influence on the relative activation barriers of the Tafel and Heyrovsky \ce{H2} production steps. Without the free energy corrections, the Heyrovsky step seems considerably more favorable than the Tafel step however, the two barriers become very similar on performing the corrections. The effect of the corrections on the barrier for $*$\ce{COOH} formation is limited and the barrier remains $\sim$1 eV.

\medskip

\begin{figure}[H]
\centering
\includegraphics[scale=0.5]{chapter_angewandte/figures/SI11_energy_COOH_coverage.png}
\caption{Change in the binding energy of $*$\ce{COOH} for different coverages of $*$\ce{OCHO} (green data points) and $*$\ce{H} (red data points). 0 eV on the y-axis corresponds to only $*$\ce{COOH} present on the Ag(110) surface.}
\label{fig:coverage_COOH}
\end{figure}

\medskip

\begin{figure}[H]
\centering
\includegraphics[scale=0.5]{chapter_angewandte/figures/SI12_energy_H_coverage.png}
\caption{Change in the binding energy of $*$\ce{H} for different coverages of $*$\ce{OCHO} (green data points) and $*$\ce{COOH} (red data points). 0 eV on the y-axis corresponds to only $*$\ce{H} present on the Ag(110) surface.}
\label{fig:coverage_H}
\end{figure}

\medskip

\newpage

\begin{figure}[p]
\begin{subfigure}{1\textwidth}
\centering
\includegraphics[scale=0.5]{chapter_angewandte/figures/SI13a_energy_H_coverage_OCHO_1.png}
\caption{}
\label{fig:fig13a}
\end{subfigure}
\begin{subfigure}{1\textwidth}
\centering
\includegraphics[scale=0.5]{chapter_angewandte/figures/SI13b_energy_H_coverage_COOH_1.png}
\caption{}
\label{fig:fig13b}
\end{subfigure}
\caption{Change in the binding energy of $*$\ce{H} in the presence of $*$\ce{OCHO} (a) and $*$\ce{COOH} (b) for Ag(110), Ag(211) and Ag(111) surfaces. The binding free energy value for 0 coverage of $*$\ce{OCHO}/$*$\ce{COOH} corresponds to the binding energy of *H on the respective surfaces.}
\label{fig:fig13}
\end{figure}

\medskip

A coverage of 1 for $*$\ce{OCHO} in Figure \ref{fig:fig13a} corresponds to a bi-dentate configuration on on-top sites across the short-bridge for a 2x3x6 (110) surface, a bi-dentate configuration on step sites for a 3x3x4 (211) surface and a bi-dentate configuration on on-top sites for a 3x3x4 (111) surface. Correspondingly, A coverage of 1 for $*$\ce{COOH} in Figure \ref{fig:fig13b} corresponds to a bi-dentate (C and O bound) configuration on on-top sites across the long-bridge for a 2x3x6 (110) surface, a bi-dentate (C and O bound) configuration on step sites for a 3x3x4 (211) surface and a mono-dentate (C bound) configuration on on-top sites for a 3x3x4 (111) surface. $*$\ce{H} is present on the short-bridge site for (110), the bridge of the step for (211) and in an fcc hollow site for (111). These configurations are found to be the most stable for the $*$\ce{OCHO}, $*$\ce{COOH} and $*$\ce{H} on the respective surfaces. The most stable adsorbate configurations on Ag(111) are used for Figure \ref{fig:G_cover_111}. Geometrical optimization is performed on all configurations as described previously. 

\medskip

It can be seen from Figure \ref{fig:fig13a} that the effect of the presence of $*$\ce{OCHO} on the binding energy of $*$\ce{H} is highly pronounced on a (110) surface and the influence diminishes as the surface becomes less active in the order (110)>(211)>(111) (see Figure \ref{fig:111_211_110}). However, the trend in the change in $\Delta G_{*H}$ due to the presence of $*$\ce{COOH} does not seem to correlate with the binding strength of adsorbates with the facet and is similar for (110), (211) and (111) surfaces (Figure \ref{fig:fig13b}).

\medskip

\begin{figure}[p]
\centering
\includegraphics[scale=0.7]{chapter_angewandte/figures/SI14_G_cover_111.pdf}
\caption{Free energy diagram for formation of \ce{CO(g)}, \ce{HCOO^-(aq)} and \ce{H2(g)} on Ag(111) surface at 0 V vs. RHE. The adsorption energies shown in dark red, blue and green are in the presence of $*$\ce{OCHO} ($\theta$=2/9), whereas the energies in light red, blue and green are without $*$\ce{OCHO}. The upward arrows denote the change in free energy of the respective intermediates due to the presence of $*$\ce{OCHO} on the surface. Formation of $*$\ce{OCHO} has been shown to occur following the formation of $*$\ce{H} as per the proposed mechanism and is not an electron transfer step.  Limiting potentials ($U_L$) are given vs. RHE.}
\label{fig:G_cover_111}
\end{figure}


\begin{table}[H]
\centering
\begin{tabular}{c|c}

\hline

$*X$  &  $E_{solv}$ in eV\\

\hline

$CO_2$ + $*$\ce{H} + 2$H_2O$ & -0.32\\
$*$\ce{COOH} & -0.19\\
$*$\ce{COOH} + 2$H_2O$ & -0.42\\
$*$\ce{H} & -0.04\\
2$*$\ce{H} + 2$H_2O$ & -0.54\\
$*$\ce{CO} & -0.02\\
$*$\ce{OH} & -0.04\\
$*$\ce{OCHO} & -0.23\\
$*$\ce{HCOO} ($I_1$) & -0.70\\
$*$\ce{CO} + $*$\ce{OH} & -0.05\\
$*$\ce{C} & -0.06\\
$*$\ce{CH} & -0.01\\
$*$\ce{CH2} & -0.01\\
$*$\ce{CH2OH} & -0.19\\
$*$\ce{CH3} & -0.04\\
$*$\ce{CHO} & -0.1\\
$*$\ce{CHOH} & -0.05\\
$*$\ce{COH} & -0.31\\
$*$\ce{OCH2} & -0.17\\
$*$\ce{OCH2O} & -0.19\\
$*$\ce{OCH3} & -0.09\\
$*$\ce{OCHO} + $*$\ce{COOH} & -0.37\\

\hline

TS species  &  $E_{solv}$ in eV\\

\hline

$*$\ce{COOH^{TS}} & $-0.28^{**}$\\
$*$\ce{OCHO^{TS}} & -0.59\\
%$*OCHO^{TS_2}$ & -0.71\\
$H_{2_{tafel}}^{TS}$ & -0.02\\
$H^{TS}_{2_{heyrovsky}}$ & $-0.42^{**}$\\

\hline
\end{tabular}
\caption{Solvation energies for intermediate and transition state species calculated using implicit solvation method. **Include the two participating explicit water molecules.}
\label{tab_SI:solv}
\end{table}

\references{chapter_angewandte/bib_angewandte}