\chapter{GMPNP Model}
\label{EES_appendix}
%two more additions needed to the appendix:
%A detailed discussion on the alternative approach using Butler Volmer equations and conservation of charge and current in the electrode+electrolyte system
%An introduction to numerically solving the nature of PDEs solved here and the challenges+solutions. revisit this again for the 3D case and point out the differences due to increase in dimensionality.

%% The following annotation is customary for chapter which have already been
%% published as a paper.
\blfootnote{Parts of this appendix have been published as supplementary information in Energy Environ. Sci. \textbf{12}, 3380 (2019) \cite{EES_divya}.

Research work led by D. Bohra; J. H. Chaudhry provided guidance for the numerical simulations; T. Burdyny and E. A. Pidko played supervisory role; W. A. Smith supervised the project as the principal investigator.}

\section{Model details}

\medskip 

\subsection{Simulation domain}
\begin{figure}[H]
    \centering
    \includegraphics[width=11cm]{chapter_EES/figures/GMPNP_CO2ER_simple.png}
    \caption{}
    \label{fig:sim_domain}
\end{figure}

\newpage

\subsection{Calculating bulk concentrations}\label{bulk_conc_calc}

We assume Henry's law to be valid for \ce{CO2} gas and calculate its concentration in water using equation (\ref{eq:Henry}). We assume the fugacity of \ce{CO2} to be 1 bar.

\begin{equation}
    C_{CO2,aq}^0 = K_H^0 C_{CO2,g}
    \label{eq:Henry}
\end{equation}

\medskip

\noindent where $K_H^0$ is Henry's constant and is given as a function of temperature T by equation (\ref{eq:Henry_constant}) \cite{ocean_acid}. The temperature is assumed to be 298.15 K for our calculations.

\begin{equation}
    \ln K_{H}^0 = 93.4517 * \left(\frac{100}{T}\right) - 60.2409 + 23.3585 * \ln \left(\frac{T}{100}\right)
    \label{eq:Henry_constant}
\end{equation}

\medskip

The saturated concentration of \ce{CO2} in an electrolyte ($C_{CO2,aq}$) with 0.1 M \ce{KHCO3} is then given by equation (\ref{eq:Sechenov1}) \cite{Sechonov}.

\begin{equation}
    \log \left(\frac{C_{CO2,aq}^0}{C_{CO2,aq}}\right) = K_SC_{S}
    \label{eq:Sechenov1}
\end{equation}

\medskip

\noindent where $C_{S}$ is the molar concentration of the electrolyte (0.1) and $K_S$ is the Sechenov's constant and can be estimated using parameters $h_i$ for species $i$. Values of $h$ for all species can be found in the Parameters section.

\begin{equation}
    K_S = \sum(h_{CO2} + h_{ion})
    \label{eq:Sechenov2}
\end{equation}
\begin{equation}
    h_{CO2} = h_{CO2}^0 + h_{CO2}^T (T - 298.15)
    \label{eq:Sechenov3}
\end{equation}
    
\medskip

In order to calculate the concentration of solution species in the bulk electrolyte, the Sechenov equation (\ref{eq:Sechenov1}) is used to estimate the saturated concentration of \ce{CO2} in a 0.1 M \ce{KHCO3} electrolyte. The estimated \ce{CO2} concentration is then used to solve the rate equations (\ref{eq:hom1}) to (\ref{eq:hom5}) (corresponding to equations (\ref{rxn:water_diss}), (\ref{rxn:bicarbonate-carbonate}) and (\ref{rxn:CO2-bicarbonate}) in the chapter \ref{EES}) till steady state is reached. 

\begin{equation}
    R_{H^+} = \frac{\partial C_{H^+}}{\partial t} =  - k_{w2}C_{H^+}C_{OH^-} + k_{w1}
    \label{eq:hom1}
\end{equation}
\begin{equation*}
    R_{OH^-} = \frac{\partial C_{OH^-}}{\partial t} = - k_{w2}C_{H^+}C_{OH^-} - k_{a1}C_{OH^-}C_{HCO_3^-}
\end{equation*}
\begin{equation}
    - k_{b1}C_{CO_2}C_{OH^-} + k_{w1} + k_{a2}C_{CO_3^{2-}} + k_{b2}C_{HCO_3^-}
\end{equation}
\begin{equation*}
    R_{HCO_3^-} = \frac{\partial C_{HCO_3^-}}{\partial t} =
\end{equation*}
\begin{equation}
    - k_{a1}C_{OH^-}C_{HCO_3^-} - k_{b2}C_{HCO_3^-} + k_{a2}C_{CO_3^{2-}} + k_{b1}C_{CO_2}C_{OH^-}
\end{equation}
\begin{equation}
    R_{CO_3^{2-}} = \frac{\partial C_{CO_3^{2-}}}{\partial t} = - k_{a2}C_{CO_3^{2-}} + k_{a1}C_{OH^-}C_{HCO_3^-}
\end{equation}
\begin{equation}
    R_{CO_2} = \frac{\partial C_{CO_2}}{\partial t} = - k_{b1}C_{CO_2}C_{OH^-} + k_{b2}C_{HCO_3^-}
    \label{eq:hom5}
\end{equation}

\medskip

The resulting bulk species concentrations for a 0.1 M \ce{KHCO3} electrolyte saturated with \ce{CO2} at 1 bar and room temperature are (in mM): $C^0_{CO2}$ = 34.061, $C^0_{CO_3^{2-}}$ = 0.039, $C^0_{H^+}$ = 0.00014, $C^0_{HCO_3^-}$ = 99.920, $C^0_{K^+}$ = 100.0, $C^0_{OH^-}$ = 7.1e-05, pH = 6.853.

\medskip

\subsection{Deviation from equilibrium}
Equilibrium constants for the reactions (\ref{rxn:water_diss}), (\ref{rxn:bicarbonate-carbonate}) and (\ref{rxn:CO2-bicarbonate}) in chapter \ref{EES} are defined as:

\begin{equation*}
    Keq_w = \frac{k_{w1}}{k_{w2}}
    \quad and \quad
    Keq_a = \frac{k_{a1}}{k_{a2}}
    \quad and \quad
    Keq_b = \frac{k_{b1}}{k_{b2}}
\end{equation*}

\medskip

The deviation of the homogeneous reactions from their equilibrium is then defined as:

\begin{equation}
    dev_{water-dissociation} = 1 - \frac{[\ce{H+}][\ce{OH-}]}{Keq_w}
    \label{eq:dev_w}
\end{equation}
\begin{equation}
    dev_{bicarbonate-carbonate} = 1 - \frac{[\ce{CO3^{2-}}]}{[\ce{HCO3-}][\ce{OH-}]Keq_a}
    \label{eq:dev_a}
\end{equation}
\begin{equation}
    dev_{CO2-bicarbonate} = 1 - \frac{[\ce{HCO3-}]}{[\ce{CO2}][\ce{OH-}]Keq_b}
    \label{eq:dev_b}
\end{equation}

\medskip

\subsection{Limiting \ce{H+} current case}\label{limiting_proton}

In the limiting \ce{H+} current case, a proton consumption current is added to the overall current such that only $<$10\% of the bulk proton concentration is allowed to be present at the OHP at steady state.  The \ce{H+} at the OHP is consumed in the heterogeneous reactions:

\begin{equation}
\cee{2H+ + 2e- <=> H2(g)}
\label{rxn:proton_consumption}
\end{equation}
\begin{equation}
\cee{CO2(aq) + 2H+ + 2e- <=> CO(g) + H2O}
\label{rxn:CO2_proton_consumption}
\end{equation}

\medskip

The \ce{OH-} and \ce{H+} flux at the OHP (x=0) then becomes:

\begin{equation}
    \vec{J}_{OH^-}|_{x=0, t} = - \frac{j_{tot}}{F} \times (1 - j_{H^+frac})
    \label{OH_flux_H_flux}
\end{equation}
\begin{equation}
    \vec{J}_{H^+}|_{x=0, t} = \frac{j_{tot}}{F} \times j_{H^+frac}
    \label{H_flux}
\end{equation}

\medskip

\noindent where $j_{H^+frac}$ is the fraction of the total current density due to \ce{H+} consumption. The \ce{CO2} flux remains the same as in equation (\ref{eq:CO2_flux}) in chapter \ref{EES}.

\medskip

\subsection{Scaling the 1D GMPNP equations}\label{scaled_mpnp}

We scale the GMPNP equations (\ref{mass_balance}), (\ref{flux_mpnp}) and (\ref{Poisson}) in chapter \ref{EES} and write them using dimensionless variables as follows:

\begin{equation}
    \frac{1}{\Lambda_D}\frac{{\partial {\tilde{C_i}}}}{\partial \tau} = \nabla \cdot \left( \nabla {\tilde{C_i}} + {\tilde{C_i}}z_i\nabla \Phi + {\tilde{C_i}}\left( \frac{\sum_{i=1}^{n} \upsilon_i \nabla {\tilde{C_i}}}{1 - \sum_{i=1}^{n-1} \upsilon_i {\tilde{C_i}}} \right) \right) + \sum_{p} {\vartheta_{ip}} R_i
    \label{mpnp_scale}
\end{equation}
\begin{equation}
    \nabla \cdot \left(\varepsilon_r \nabla \Phi \right) = -q \sum_{i=1}^{n} z_i{\tilde{C_i}}
    \label{Poisson_scale}
\end{equation}
\begin{equation}
    L_{debye} = \sqrt{\frac{\varepsilon_0\varepsilon_r^0k_BT}{2e_0^2C_{elec}N_A}}
    \label{eq:debye}
\end{equation}

\[\nabla = \frac{\partial}{\partial \tilde{x}} , \Lambda_D = \frac{L_{debye}}{L_n}, \tilde{x} = \frac{x}{L_n} , \tau = \frac{tD_i}{L_{debye}L_n} , \tilde{C_i} = \frac{C_i}{C_i^0} , 
\]
\medskip
\[\Phi = \frac{\phi F}{RT}
 , {\vartheta_{ip}} = \frac{L_n^2}{D_iC^0_i} ,\upsilon_i = a_i^3N_AC^0_i,  q = \frac{(FL_n)^2C^0_i}{\varepsilon_0RT}
\]

\medskip

\noindent where $L_n$ is the system length which is assumed to be 50 $\mu$m, $L_{debye}$ is the Debye length as defined by equation (\ref{eq:debye}), $C_{elec}$ is the bulk concentration of the electrolyte, $e_0$ is the fundamental charge of electron and $C^0_i$ is the bulk concentration of species $i$. The equations (\ref{mpnp_scale}) and (\ref{Poisson_scale}) are solved simultaneously with the appropriate initial and boundary conditions to obtain the species concentration and potential profiles at steady state. 

\medskip

\subsection{Scaling the 3D GMPNP equations}\label{scaled_mpnp_3D}

Below are the scaled MPNP and Poisson equations corresponding to equations (\ref{mass_balance_3D}), (\ref{flux_mpnp_3D}) and (\ref{Poisson_3D}) in chapter \ref{chapter_GDE_pore}.

\begin{equation}
    \frac{{\partial {\tilde{C_i}}}}{\partial \Gamma} = \tilde{\nabla} \cdot \left( \tilde{\nabla} {\tilde{C_i}} + {\tilde{C_i}}z_i\tilde{\nabla} \Phi + {\tilde{C_i}}\left( \frac{\sum_{i=1}^{n} \upsilon_i \tilde{\nabla} {\tilde{C_i}}}{1 - \sum_{i=1}^{n} \upsilon_i {\tilde{C_i}}} \right) \right) + \sum_{j} {\vartheta_{i}} R_{ij}
    \label{mpnp_scale_3D}
\end{equation}

\begin{equation}
    \tilde{\nabla} \cdot \left(\varepsilon_r \tilde{\nabla} \Phi \right) = -\sum_{i=1}^{n} q_i z_i{\tilde{C_i}}
    \label{Poisson_scale_3D}
\end{equation}

\[\Lambda = \frac{R}{L}, \tilde{x} = \frac{x}{L} , \tilde{y} = \frac{y}{L}, \tilde{z} = \frac{z}{L}, \Gamma = \frac{tD_i^{eff}}{L^2} , \tilde{C_i} = \frac{C_i}{C_i^0} , 
\]
\medskip
\[\Phi = \frac{\phi F}{R_GT}
 , {\vartheta_{i}} = \frac{L^2}{D_i^{eff}C^0_i} ,\upsilon_i = a_i^3N_AC^0_i,  q_i = \frac{(FL)^2C^0_i}{\varepsilon_0R_GT}
\]

\begin{equation}
    \tilde{\nabla} u =  \frac{\partial u}{\partial \tilde{x}}\vec{e_x} + \frac{\partial u}{\partial \tilde{y}}\vec{e_y} + \frac{\partial u}{\partial \tilde{z}}\vec{e_z}
    \label{eq:gradient_scaled}
\end{equation}

\begin{equation}
    \tilde{\nabla^2} u = \frac{\partial^2 u}{\partial \tilde{x}^2}\vec{e_x} + \frac{\partial^2 u}{\partial \tilde{y}^2}\vec{e_y} + \frac{\partial^2 u}{\partial \tilde{z^2}}\vec{e_z}
    \label{eq:laplacian_scaled}
\end{equation}

\medskip

\noindent where $L$ is the length and $R$ is the radius of the cylindrical pore with OHP as the outer cylindrical boundary and $C^0_i$ is the bulk concentration of species $i$. Scaling all length dimensions with the same constant ($L$) leads to a simpler computational implementation of the weak form in FEniCS.

\medskip

Consequently, the equations (\ref{mpnp_scale_3D}) and (\ref{Poisson_scale_3D}) need to be solved simultaneously with the appropriate initial and boundary conditions to obtain the species concentration and potential profiles at steady state. 

\medskip

\subsubsection{Galerkin form in 3D}

In order to solve the GMPNP equations using FEniCS, we need to derive the Galerkin (or the weak form) of the non-linear partial differential equations (\ref{mpnp_scale_3D}) and (\ref{Poisson_scale_3D}). We use an implicit Euler time discretization with the volume element $dV$ defined as $d\tilde{x}d\tilde{y}d\tilde{z}$ and gradient defined for the scaled cartesian coordinates as given in equation (\ref{eq:gradient_scaled}). $p_i$ and $s$ are the the test functions for $\tilde{C_i}$ and $\Phi$ in the Galerkin form, respectively. The surface integral term becomes zero in case of a Dirichlet boundary condition or is defined using the surface fluxes as a Neumann boundary condition. In equation (\ref{eq:Galerkin_potential}), \textbf{n} is the outward normal direction to the respective surface.

\begin{equation*}
    \int\limits_V \left(\frac{\tilde{C}_i^{n+1} - \tilde{C}^n_i}{\Delta\Gamma}\right)p_idV 
\end{equation*}
\begin{equation*}
     + \int\limits_V\left[\tilde{\nabla}{\tilde{C_i}}\tilde{\nabla}{p_i} + z_i\tilde{C_i}\tilde{\nabla}{\Phi}\tilde{\nabla}{p_i} + {\tilde{C_i}}\left( \frac{\sum_{i=1}^{n} \upsilon_i \tilde{\nabla} {\tilde{C_i}}}{1 - \sum_{i=1}^{n} \upsilon_i {\tilde{C_i}}} \right)\tilde{\nabla}{p_i} - p_i {\vartheta_{i}}\sum_{j} R_{ij}\right]dV 
\end{equation*}
\begin{equation}
    + \int\limits_{S_1} \mathbf{J}_i^{S_1}p_idS_1 + \int\limits_{S_2} \mathbf{J}_i^{S_2}p_idS_2 + \int\limits_{S_3} \mathbf{J}_i^{S_3}p_idS_3 = 0
    \label{eq:Galerkin_MNPE}
\end{equation}

\begin{equation*}
    \int\limits_V \left[s\sum_{i=1}^{n}q_iz_i\tilde{C_i} - \varepsilon_r\tilde{\nabla}{\Phi}\tilde{\nabla}{s}\right]dV
\end{equation*}
\begin{equation}
    + \int\limits_{S_1} \frac{\partial\Phi}{\partial n}\varepsilon_rsdS_1 + \int\limits_{S_2} \frac{\partial\Phi}{\partial n}\varepsilon_rsdS_2 + \int\limits_{S_3} \frac{\partial\Phi}{\partial n}\varepsilon_rsdS_3 = 0
    \label{eq:Galerkin_potential}
\end{equation}

\subsection{Reaction-Diffusion and PNP system of equations} \label{sec:rxn_diff_PNP}

\medskip

\subsubsection{Reaction-diffusion model}
In the reaction-diffusion model, the flux term in the mass balance equation only contains the diffusion mass transport term and excludes the migration and volume correction terms as given below.

\begin{equation}
    \frac{{\partial {C_i}}}{\partial t} = - \nabla \cdot \vec{J_i} + \sum_{p} R_i
    \label{mass_balance_RD}
\end{equation}
\begin{equation}
    \vec{J_i} = - D_i\nabla {C_i}
    \label{flux_RD}
\end{equation}

\medskip

\subsubsection{PNP equations}
The Poisson-Nernst-Planck (PNP) equations solve the dynamics of the mass transport of solution species including the effects of diffusion, reaction as well as migration such that equations (\ref{mass_balance_PNP}) and (\ref{Poisson_PNP}) are solved simultaneously. However, dilute solution theory is used and the equations are valid for point species.

\begin{equation}
    \frac{{\partial {C_i}}}{\partial t} = - \nabla \cdot \vec{J_i} + \sum_{p} R_i
    \label{mass_balance_PNP}
\end{equation}
\begin{equation}
    \vec{J_i} = - D_i\nabla {C_i} - \frac{D_iC_iz_iF}{RT}\nabla \phi
    \label{flux_pnp}
\end{equation}
\begin{equation}
    \nabla \cdot \left(\varepsilon_0 \varepsilon_r \nabla \phi \right) = - F \sum_{i=1}^{n} z_iC_i
    \label{Poisson_PNP}
\end{equation}

\medskip

Equation (\ref{eq:rel_permittivity}) in the chapter \ref{EES} is assumed to hold for the relative permittivity ($\varepsilon_r$). For both the reaction-diffusion and PNP systems, $R_i$ are as given in equations (\ref{eq:hom1}) to (\ref{eq:hom5}) and the boundary conditions used for the species concentrations are the same as that for the GMPNP system of equations.

\medskip

\subsection{SUPG Stabilization of the PNP system}\label{SUPG_PNP}
 A Streamlined Upwind Petrov-Galerkin (SUPG) stabilization was used for the PNP equations to be able to resolve the steady-state concentration and potential profiles at practically relevant applied voltages for a system of size 50 $\mu$m \cite{Zeb_SUPG,SUPG_1,SUPG_2,SUPG_3}.

\begin{equation}
    \Vec{b_i} = - z_i \nabla \Phi
    \label{velocity_field}
\end{equation}
\begin{equation}
    \sigma_{i\iota} = \sigma_{i\iota}^0 \times Pe_{i\iota}
\end{equation}

\medskip

\noindent where 

\begin{equation}
    \sigma_{i\iota}^0 = \frac{h_\iota}{2 |z_i| \|\nabla \Phi\|_2}
\end{equation}

\medskip

\noindent and

\begin{equation}
  Pe_{i\iota} =
    \begin{cases}
      \frac{h_\iota |z_i| \|\nabla \Phi\|_2}{2} & \text{if $Pe_{i\iota}$ $\leq$ 1}\\
      1 & \text{if $Pe_{i\iota}$ $>$ 1}
    \end{cases}       
\end{equation}

\medskip

\noindent $\Vec{b_i}$ is the flow field due to migration which is the equivalent dimensionless velocity term in equation (\ref{mass_balance_PNP}) and (\ref{flux_pnp}). $\sigma_{i\iota}$ and $Pe_{i\iota}$ are the stability parameter for the SUPG term and the P\'{e}clet number for species $i$ for element $\iota$ of the mesh, respectively.

\medskip

The test function of the SUPG stabilization term in the Galerkin form is then given by:

\begin{equation}
  \nu_{i\iota}^{SUPG} = \sigma_{i\iota} \Vec{b_i} \cdotp \nabla{v_i} = 
    \begin{cases}
      - \frac{h_\iota^2z_i}{4}\nabla \Phi \cdotp \nabla v_i & \text{if $Pe_{i\iota}$ $\leq$ 1}\\
      - \frac{h_\iota z_i}{2 |z_i| \|\nabla \Phi\|_2}\nabla \Phi \cdotp \nabla v_i & \text{if $Pe_{i\iota}$ $>$ 1}
    \end{cases}       
\label{SUPG_factor}
\end{equation}

\medskip

The SUPG stabilization term in its weak-form for the Nernst Planck equations (NP) is given by multiplying the test function as given by equation (\ref{SUPG_factor}) to the residual of the NP. The overall stabilized NP equation is then given by equation (\ref{SUPG_full}).

\begin{equation*}
{\int_\Omega\left(\left(\frac{\tilde{C_i}^{n+1} - \tilde{C_i}^n}{\Delta \tau \times \Lambda_{D}} \right) - \nabla \cdotp \left( \nabla {\tilde{C_i}} + {\tilde{C_i}}z_i\nabla \Phi \right) - \sum_{p} {\vartheta_{ip}} R_i\right) v_i \partial x  +}
\end{equation*}
\begin{equation}
{\sum_{\iota \epsilon I} \int_\iota\left(\left(\frac{\tilde{C_i}^{n+1} - \tilde{C_i}^n}{\Delta \tau \times \Lambda_{D}} \right) - \nabla \cdotp \left( \nabla {\tilde{C_i}} + {\tilde{C_i}}z_i\nabla \Phi \right) - \sum_{p} {\vartheta_{ip}} R_i\right) \nu_{i\iota}^{SUPG} \partial x} 
\label{SUPG_full}
\end{equation}

\medskip

\noindent where the first integral term in equation (\ref{SUPG_full}) is nothing but the residual of the scaled PNP equation multiplied by the test function $v_i$ and integrated over the entire finite element domain $\Omega$. The second term in equation (\ref{SUPG_full}) is the stabilization term which is a summation of the residual multiplied by the test function for SUPG (equation (\ref{SUPG_factor})) integrated over each element in the mesh. 

\medskip

\noindent The first term in equation (\ref{SUPG_full}) is integrated by parts to derive the weak form as is common in finite element methods. We drop the higher order differential terms in the SUPG stabilization (second term in equation (\ref{SUPG_full})) since the basis functions used for the finite element solver as piece-wise linear. The final form of the stabilized scaled NP equation is given by (\ref{SUPG_full_first_order_NPE}).

\begin{equation*}
{\int_\Omega\left(\left(\frac{\tilde{C_i}^{n+1} - \tilde{C_i}^n}{\Delta \tau \times \Lambda_{D}} \right) - \nabla \cdotp \left( \nabla {\tilde{C_i}} + {\tilde{C_i}}z_i\nabla \Phi \right) - \sum_{p} {\vartheta_{ip}} R_i\right) v_i \partial x  +}
\end{equation*}
\begin{equation}
{\sum_{\iota \epsilon I} \int_\iota \left(\left(\frac{\tilde{C_i}^{n+1} - \tilde{C_i}^n}{\Delta \tau \times \Lambda_{D}} \right) - z_i\nabla {\tilde{C_i}} \cdotp \nabla \Phi - \sum_{p} {\vartheta_{ip}} R_i \right) \nu_{i\iota}^{SUPG} \partial x}
\label{SUPG_full_first_order_NPE}
\end{equation}

\medskip

Note that although the Poisson equation is solved simultaneously with the NP equation, it does not feature in the stabilization implemented. Equation (\ref{SUPG_full_first_order_NPE}) is simultaneously solved with the Poisson equation with the initial and boundary conditions mentioned in chapter \ref{EES}.

\medskip

\section{Parameters}\label{sec:params}

\quad Potential at the point of zero charge in V vs SHE at pH=7 \cite{Trasatti1999}:

\medskip

\begin{center}
\begin{tabular}{|l|l|l|}
\hline
Surface & Value \\ \hline
Ag-pc & -0.70 \\ \hline
Ag(111) & -0.450 \\ \hline
Ag(110) & -0.735 \\ \hline
Ag(100) & -0.616 \\ \hline
\end{tabular}
\label{table:PZC_Ag}
\end{center}

\medskip

\quad Rate-constants:

\medskip

\begin{center}
\begin{tabular}{|l|l|l|c|}
\hline
Constant & Value & Units & Reference \\ \hline
kw1 & 2.4e-2 & mol$\cdot$m-3$\cdot$s-1 &  \cite{SinghE8812,Singh2015,Atkins}\\ \hline
kw2 & 2.4e+6 & mol-1$\cdot$m3$\cdot$s-1 & \cite{SinghE8812,Singh2015,Atkins}\\ \hline
ka1 & 6.0e+6 & mol-1$\cdot$m3$\cdot$s-1 &  \cite{Tom_bubbles}\\ \hline
ka2 & 1.07e+6 & s-1 & \cite{Tom_bubbles}\\ \hline
kb1 & 2.23 & mol-1$\cdot$m3$\cdot$s-1 & \cite{Tom_bubbles}\\ \hline
kb2 & 5.23e-5 & s-1 & \cite{Tom_bubbles}\\ \hline
\end{tabular}
\label{table:homo_rate_constants}
\end{center}

\medskip

Diffusion-coefficients in $m^2 \cdot s^{-1}$:

\medskip

\begin{center}
\begin{tabular}{|l|l|l|}
\hline
Constant & Value & Reference \\ \hline
D$_{H+}$ & 9.311e-9 &  \cite{SinghE8812,Singh2015} \\ \hline
D$_{OH-}$ & 5.273e-9 &  \cite{SinghE8812,Singh2015}\\ \hline
D$_{CO2}$ & 1.91e-9 &  \cite{SinghE8812,Singh2015}\\ \hline
D$_{CO}$ & 2.03e-9 &  \cite{diff_coeff_gases}\\ \hline
D$_{H2}$ & 4.5e-9 &  \cite{diff_coeff_gases}\\ \hline
D$_{HCO3-}$ & 1.185e-9 &  \cite{SinghE8812,Singh2015}\\ \hline
D$_{CO32-}$ & 0.923e-9 & \cite{SinghE8812,Singh2015} \\ \hline
D$_{K+}$ & 1.957e-9 & \cite{diff_coeff2}\\ \hline
D$_{Na+}$ & 1.334e-9 &  \cite{diff_coeff2}\\ \hline
D$_{Li+}$ & 1.029e-9 & \cite{diff_coeff2} \\ \hline
D$_{Cs+}$ & 2.06e-9 & \cite{diff_coeff2}\\ \hline
\end{tabular}  
\label{table:diff_coeff}
\end{center}

\medskip

\blfootnote{$^*$ The solvated size of \ce{HCO3-} is assumed to be similar to \ce{CO3^{2-}} due to unavailability of a reliable value in literature. $^{**}$ The solvated size of \ce{CO2}, \ce{CO} and \ce{H2} are assumed based on bond distances without solvation.}

Solvation sizes in $m$ \cite{Nightingale1959}:

\begin{center}
\begin{tabular}{|l|l|}
\hline
Constant & Value \\ \hline
a$_{H+}$ & 0.56e-9 \\ \hline
a$_{OH-}$ & 0.6e-9 \\ \hline
a$_{CO2}^{**}$ & 0.23e-9 \\ \hline
a$_{CO}^{**}$ & 0.113e-9 \\ \hline
a$_{H2}^{**}$ & 0.074e-9 \\ \hline
a$_{HCO3-}^*$ & 0.8e-9 \\ \hline
a$_{CO32-}$ & 0.788e-9 \\ \hline
a$_{K+}$ & 0.662e-9 \\ \hline
a$_{Na+}$ & 0.716e-9 \\ \hline
a$_{Li+}$ & 0.764e-9 \\ \hline
a$_{Cs+}$ & 0.658e-9 \\ \hline
\end{tabular}
\label{table:solv_size}
\end{center}

\newpage

Parameters used to estimate Sechenov's constant in $m^3 \cdot kmol^{-1}$ \cite{Sechonov}:

\medskip

\begin{center}
\begin{tabular}{|l|l|}
\hline
Constant & Value \\ \hline
h$_{K+}$ & 0.0922 \\ \hline
h$_{OH-}$ & 0.0839 \\ \hline
h$_{HCO3-}$ & 0.0967 \\ \hline
h$_{CO32-}$ & 0.1423 \\ \hline
h$_{CO2}^0$ & -0.0172 \\ \hline
h$_{CO2}^T$ & -0.000338 \\ \hline
\end{tabular}
\label{table:hydration_number}
\end{center}

\medskip

Hydration numbers for cations  \cite{Bockrisbook,Bockris_solvation}:

\medskip

\begin{center}
\begin{tabular}{|l|l|}
\hline
Constant & Value \\ \hline
w$_{K+}$ & 4 \\ \hline
w$_{Li+}$ & 5 \\ \hline
w$_{Na+}$ & 5 \\ \hline
w$_{Cs+}$ & 3 \\ \hline
w$_{H+}$ & 10 \\ \hline
\end{tabular}
\end{center}

\medskip

Henry's constants for gases in water in $mol \cdot kg^{-1} \cdot bar^{-1}$  \cite{Henry}:

\medskip

\begin{center}
\begin{tabular}{|l|l|}
\hline
Constant & Value \\ \hline
H$_{CO2}$ & 0.034 \\ \hline
H$_{CO}$ & 0.00095 \\ \hline
H$_{H2}$ & 0.00078 \\ \hline
\end{tabular}
\end{center}

\medskip

Other constants 

\medskip

\begin{center}
\begin{tabular}{|l|l|l|c|}
\hline
Constant & Value & Units \\ \hline
$\rho_e$ & 997 & kg$\cdot$m-3 \\ \hline
$\mu_e$ & 0.89e-3 & kg$\cdot$m-1$\cdot$s-1 \\ \hline
$L_c$ & 1.5e-2 & m \\ \hline
$\mathbf{v}_e$ & 0.25e-6 & m3$\cdot$s-1 \\ \hline
$A_{cross}$ & 1.5e-4 & m2 \\ \hline
$L_{cross}$ & 1.0e-2 & m \\ \hline
\end{tabular}
\label{table:other_constants}
\end{center}
    
\newpage

\section{Supplementary Results: 1D GMPNP model}\label{SI_sec:1D_results}

\begin{figure}[h]
\centering
\begin{subfigure}{.5\textwidth}
    \centering
    \includegraphics[width=5cm]{chapter_EES/figures/OH_x.png}
    \caption{}
\end{subfigure}%
\begin{subfigure}{0.5\textwidth}
    \centering
    \includegraphics[width=5cm]{chapter_EES/figures/E_x.png}
    \caption{}
\end{subfigure}
\begin{subfigure}{.5\textwidth}
    \centering
    \includegraphics[width=5cm]{chapter_EES/figures/HCO3_x.png}
    \caption{}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
    \centering
    \includegraphics[width=5cm]{chapter_EES/figures/eps_r_x.png}
    \caption{}
\end{subfigure}
\caption{The electrical double layer (EDL) facing a planar \ce{CO2}ER catalyst for a 0.1 M \ce{KHCO3} electrolyte solution saturated with \ce{CO2}. The above results are derived for a total current density of 1 mA/cm$^2$ and a CO Faradaic efficiency of 0.8. PZC stands for the potential of point of zero charge of the planar catalyst surface and x=0 is the OHP.}
\end{figure}

\newpage

\begin{figure}[H]
\centering
\begin{subfigure}{.5\textwidth}
    \centering
    \includegraphics[width=5cm]{chapter_EES/figures/HCO3_x_MPNP_PNP_rxn_diff.png}
    \caption{}
\end{subfigure}%
\begin{subfigure}{0.5\textwidth}
    \centering
    \includegraphics[width=5cm]{chapter_EES/figures/CO32_x_MPNP_PNP_rxn_diff.png}
    \caption{}
\end{subfigure}
\caption{Comparison between results obtained from reaction-diffusion (RD) model, a Poisson-Nernst-Planck (PNP) model and a generalized modified PNP (GMPNP) model for the EDL region. x=0 is located at the OHP. All results have been derived for a total current density of 1 mA/cm$^2$ and a CO Faradaic efficiency of 0.8. The PNP and GMPNP results are for a voltage of -0.32 V vs PZC at the OHP.}
\end{figure}

\medskip

\begin{figure}[H]
\centering
\begin{subfigure}{.33\textwidth}
    \centering
    \includegraphics[width=3.3cm]{chapter_EES/figures/CO2_MPNP_multi_current_close.png}
    \caption{GMPNP: EDL}
\end{subfigure}%
\begin{subfigure}{0.33\textwidth}
    \centering
    \includegraphics[width=3.3cm]{chapter_EES/figures/CO2_MPNP_multi_current.png}
    \caption{GMPNP: Full domain}
\end{subfigure}%
\begin{subfigure}{0.33\textwidth}
    \centering
    \includegraphics[width=3.3cm]{chapter_EES/figures/CO2_RD_multi_current.png}
    \caption{RD: Full domain}
\end{subfigure}
\caption{Influence of total current density on \ce{CO2} concentration derived using the GMPNP model. Figure (a) shows the profiles for a region of 10 nm from the OHP whereas Figure (b) shows the profiles for the entire Nernst layer extending to 50 $\mu$m. Figure (c) shows results obtained using reaction-diffusion (RD) model for the purpose of comparison. All results are calculated for a CO Faradaic efficiency of 0.8 and the GMPNP results are for a potential of -0.32 V vs PZC at the OHP.}
\end{figure}

\medskip

\begin{figure}[H]
\centering
\begin{subfigure}{.5\textwidth}
    \centering
    \includegraphics[width=5cm]{chapter_EES/figures/pH_MPNP_multi_current_close_limiting_H.png}
    \caption{}
\end{subfigure}%
\begin{subfigure}{0.5\textwidth}
    \centering
    \includegraphics[width=5cm]{chapter_EES/figures/pH_MPNP_multi_current_limiting_H.png}
    \caption{}
\end{subfigure}
\caption{Influence of total current density on pH derived using the GMPNP model for the limiting \ce{H+} current case where no more than 10\% of the bulk proton concentration is allowed to be present at the OHP (x=0). Figure (a) shows the profiles for a region of 10 nm from the OHP whereas Figure (b) shows the profiles for the entire Nernst layer extending to 50 $\mu$m. All results are calculated for a CO Faradaic efficiency of 0.8 and for a potential of -0.32 V vs PZC at the OHP.}
\label{fig:limiting_current}
\end{figure}

\medskip

\begin{figure}[H]
    \centering
    \includegraphics[width=6cm]{chapter_EES/figures/cation_size_illus.png}
    \caption{Illustration of the qualitative difference in the concentrations and potential screening lengths of a small solvated cation vs. a large solvated cation acting as counter-ions in the EDL. The red dashed line represents the OHP. $C_i$ and $d_i$ are concentration at the OHP at the steric limit and the width of the condensed region of the EDL, respectively for cation $i$.}
    \label{fig:cation_illus}
\end{figure}

\medskip

\begin{figure}[H]
\centering
\begin{subfigure}{.5\textwidth}
    \centering
    \includegraphics[width=5cm]{chapter_EES/figures/eps_r_cation_x.png}
    \caption{}
\end{subfigure}%
\begin{subfigure}{0.5\textwidth}
    \centering
    \includegraphics[width=5cm]{chapter_EES/figures/pH_cation_x.png}
    \caption{}
\end{subfigure}
\caption{Effect of cation size on the relative permittivity and pH of the EDL. Calculations are performed for a potential of -0.32 V vs PZC at the OHP for a total current density of 1 mA/cm$^2$ and a CO Faradaic efficiency of 0.8.}
\end{figure}

\newpage

\section{Supplementary Results: 3D GMPNP model}\label{SI_sec:3D_results}

\begin{figure}[H]
\centering
\includegraphics[width=7cm]{chapter_GDE_pore/figures/L_ECSA/CO32.png}
\caption{Influence of changing length ($L$) of effective catalyst pore on median carbonate ion concentration within the pore. The radius ($R$) of all pores is 5 nm.}
\label{fig:L_ECSA_CO32}
\end{figure}

\medskip

\begin{figure}[H]
\centering
\begin{subfigure}{.33\textwidth}
    \centering
    \includegraphics[width=4cm]{chapter_GDE_pore/figures/D_eff/pH.png}
    \caption{}
\end{subfigure}%
\begin{subfigure}{0.33\textwidth}
    \centering
    \includegraphics[width=4cm]{chapter_GDE_pore/figures/D_eff/CO2.png}
    \caption{}
\end{subfigure}%
\begin{subfigure}{0.33\textwidth}
    \centering
    \includegraphics[width=4cm]{chapter_GDE_pore/figures/D_eff/charged_species.png}
    \caption{}
\end{subfigure}
\caption{Influence of changing the ratio of effective to bulk diffusion coefficient of electrolyte species on their median concentration in pore. A pore of 5 nm x 100 nm ($R$ x $L$) and $I_{ECSA}$=2 mA/cm$^2$ is used for all data points.}
\label{fig:D_eff}
\end{figure}

\medskip

\begin{figure}[H]
\centering
\begin{subfigure}{.33\textwidth}
    \centering
    \includegraphics[width=4cm]{chapter_GDE_pore/figures/L_geom_ECSA/HCO3.png}
    \caption{}
\end{subfigure}%
\begin{subfigure}{0.33\textwidth}
    \centering
    \includegraphics[width=4cm]{chapter_GDE_pore/figures/L_geom_ECSA/CO32.png}
    \caption{}
\end{subfigure}%
\begin{subfigure}{0.33\textwidth}
    \centering
    \includegraphics[width=4cm]{chapter_GDE_pore/figures/L_geom_ECSA/CO2.png}
    \caption{}
\end{subfigure}
\caption{A comparison between median species concentrations within pore derived for constant $I_{geom}$ \textit{vs.} constant $I_{ECSA}$. The annotations on the red dotted curve signify the value of $I_{ECSA}$ corresponding to the respective pore size for the constant $I_{geom}$. The radius ($R$) of all pores is 5 nm.}
\label{fig:SI_L_geom_ECSA}
\end{figure}

\medskip

\begin{figure}[H]
\centering
\includegraphics[width=6.5cm]{chapter_GDE_pore/figures/Reynolds_no/CO2.png}
\caption{Effect of changing Reynolds number on \ce{CO2} concentration in the catalyst pore. A pore of 5 nm x 100 nm ($R$ x $L$) and $I_{ECSA}$=2 mA/cm$^2$ is used for all data points.}
\label{fig:Reynolds_CO2}
\end{figure}

\medskip

\begin{figure}[H]
\centering
\begin{subfigure}{.33\textwidth}
    \centering
    \includegraphics[width=4cm]{chapter_GDE_pore/figures/press_gas/pH.png}
    \caption{}
    \label{fig:press_gas_pH}
\end{subfigure}%
\begin{subfigure}{0.33\textwidth}
    \centering
    \includegraphics[width=4cm]{chapter_GDE_pore/figures/press_gas/CO2.png}
    \caption{}
    \label{fig:press_gas_CO2}
\end{subfigure}%
\begin{subfigure}{0.33\textwidth}
    \centering
    \includegraphics[width=4cm]{chapter_GDE_pore/figures/press_gas/charged_species.png}
    \caption{}
    \label{fig:press_gas_charged}
\end{subfigure}
\caption{Influence of changing the total gas pressure at the gas-electrolyte interface on the median concentration of electrolyte species in the pore. A pore of 5 nm x 100 nm ($R$ x $L$) and $I_{ECSA}$=2 mA/cm$^2$ is used for all data points.}
\label{fig:press_gas}
\end{figure}

\medskip

\begin{figure}[H]
\centering
\includegraphics[width=7cm]{chapter_GDE_pore/figures/L_rxn_diff_MPNP/HCO3.png}
\caption{Comparing the median concentration of \ce{HCO3-} in the catalyst pore with changing pore length ($L$) derived using a reaction-diffusion model and a GMPNP model. A pore of 5 nm x 100 nm ($R$ x $L$) and $I_{ECSA}$=2 mA/cm$^2$ is used for all data points.}
\label{fig:L_rxn_diff_MPNP_HCO3}
\end{figure}

\begin{figure}[p]
\centering
\begin{subfigure}{1.0\textwidth}
    \centering
    \caption{Common scale}
    \includegraphics[width=9cm]{chapter_GDE_pore/figures/pore_scatter_rxn_diff/pH_compare.png}
    \label{fig:pore_scatter_pH_common_scale}
\end{subfigure}
\begin{subfigure}{1.0\textwidth}
    \centering
    \caption{GMPNP}
    \includegraphics[width=9cm]{chapter_GDE_pore/figures/pore_scatter_MPNP/pH.png}
    \label{fig:pore_scatter_pH_MPNP}
\end{subfigure}
\begin{subfigure}{1.0\textwidth}
    \centering
    \caption{RD}
    \includegraphics[width=9cm]{chapter_GDE_pore/figures/pore_scatter_rxn_diff/pH.png}
    \label{fig:pore_scatter_pH_rxn_diff}
\end{subfigure}
\caption{A comparison of pH along a longitudinal cross-section of a pore with $R$ x $L$ of 5 nm x 100 nm and $I_{ECSA}$=2 mA/cm$^2$ for the GMPNP and reaction-diffusion (RD) model. An applied potential of -25 mV vs PZC for the catalyst surface is used for the GMPNP model. The gas-liquid interface is the left edge whereas the electrolyte-electrolyte interface is the right edge with the top and bottom edges being part of the catalyst surface. (a) shows the output of the GMPNP and RD model on the same scale for comparison. (b) and (c) show the distribution on separate scales for clarity.}
\label{fig:pore_scatter_pH}
\end{figure}


\begin{figure}[p]
\centering
\begin{subfigure}{1.0\textwidth}
    \centering
    \caption{Common scale}
    \includegraphics[width=9cm]{chapter_GDE_pore/figures/pore_scatter_rxn_diff/CO32_compare.png}
    \label{fig:pore_scatter_CO32_common_scale}
\end{subfigure}
\begin{subfigure}{1.0\textwidth}
    \centering
    \caption{GMPNP}
    \includegraphics[width=9cm]{chapter_GDE_pore/figures/pore_scatter_MPNP/CO32.png}
    \label{fig:pore_scatter_CO32_MPNP}
\end{subfigure}
\begin{subfigure}{1.0\textwidth}
    \centering
    \caption{RD}
    \includegraphics[width=9cm]{chapter_GDE_pore/figures/pore_scatter_rxn_diff/CO32.png}
    \label{fig:pore_scatter_CO32_rxn_diff}
\end{subfigure}
\caption{A comparison of \ce{CO3^{2-}} concentration along a longitudinal cross-section of a pore with $R$ x $L$ of 5 nm x 100 nm and $I_{ECSA}$=2 mA/cm$^2$ for the GMPNP and reaction-diffusion (RD) model. An applied potential of -25 mV vs PZC for the catalyst surface is used for the GMPNP model. The gas-liquid interface is the left edge whereas the electrolyte-electrolyte interface is the right edge with the top and bottom edges being part of the catalyst surface. (a) shows the output of the GMPNP and RD model on the same scale for comparison. (b) and (c) show the distribution on separate scales for clarity.}
\label{fig:pore_scatter_CO32}
\end{figure}


\begin{figure}[p]
\centering
\begin{subfigure}{1.0\textwidth}
    \centering
    \caption{Common scale}
    \includegraphics[width=9cm]{chapter_GDE_pore/figures/pore_scatter_rxn_diff/OH_compare.png}
    \label{fig:pore_scatter_OH_common_scale}
\end{subfigure}
\begin{subfigure}{1.0\textwidth}
    \centering
    \caption{RD}
    \includegraphics[width=9cm]{chapter_GDE_pore/figures/pore_scatter_rxn_diff/OH.png}
    \label{fig:pore_scatter_OH_rxn_diff}
\end{subfigure}
\caption{A comparison of \ce{OH-} concentration along a longitudinal cross-section of a pore with $R$ x $L$ of 5 nm x 100 nm and $I_{ECSA}$=2 mA/cm$^2$ for the GMPNP and reaction-diffusion (RD) model. An applied potential of -25 mV vs PZC for the catalyst surface is used for the GMPNP model. The gas-liquid interface is the left edge whereas the electrolyte-electrolyte interface is the right edge with the top and bottom edges being part of the catalyst surface. (a) shows the output of the GMPNP and RD model on the same scale for comparison. (b) shows the distribution on a separate scales for the RD model for clarity.}
\label{fig:pore_scatter_OH}
\end{figure}


\begin{figure}[p]
\centering
\begin{subfigure}{1.0\textwidth}
    \centering
    \caption{CO (aq)}
    \includegraphics[width=9cm]{chapter_GDE_pore/figures/pore_scatter_rxn_diff/CO_compare.png}
    \label{fig:pore_scatter_CO_common_scale}
\end{subfigure}
\begin{subfigure}{1.0\textwidth}
    \centering
    \caption{\ce{H2} (aq)}
    \includegraphics[width=9cm]{chapter_GDE_pore/figures/pore_scatter_rxn_diff/H2_compare.png}
    \label{fig:pore_scatter_H2_rxn_diff}
\end{subfigure}
\caption{A comparison of dissolved \ce{CO} (a) and \ce{H2} (b) concentrations along a longitudinal cross-section of a pore with $R$ x $L$ of 5 nm x 100 nm and $I_{ECSA}$=2 mA/cm$^2$ for the GMPNP and reaction-diffusion (RD) model. An applied potential of -25 mV vs PZC for the catalyst surface is used for the GMPNP model. The gas-liquid interface is the left edge whereas the electrolyte-electrolyte interface is the right edge with the top and bottom edges being part of the catalyst surface.}
\label{fig:pore_scatter_products}
\end{figure}

\newpage

\references{chapter_EES/bib_EES}