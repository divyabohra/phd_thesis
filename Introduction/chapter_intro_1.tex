\chapter{Electrochemical reduction of \ce{CO2}}
\label{intro_1}

\section{Climate change and \ce{CO2}}

The element carbon (C) is fundamental for the existence of all life on Earth. Its flow through our planet's landmass, geology, oceans, and atmosphere forms a closed loop known as the \emph{carbon cycle}. \emph{Greenhouse gases} (GHG) such as carbon dioxide (\ce{CO2}), methane (\ce{CH4}), water vapour, nitrous oxide (\ce{N2O}), and ozone, absorb the thermal radiation which is reflected from the earth's surface and reflect it back into the atmosphere. Without the warming effect of GHGs, life on Earth would not be possible. The carbon cycle helps control the planet's temperature by regulating the quantities of \ce{CO2} and \ce{CH4} in the atmosphere.

\medskip

Burning of fossil fuels in the past century has disrupted the balance of the carbon cycle by moving large quantities of C from the ground (fossils) into the atmosphere in the form of \ce{CO2} \cite{IPCC2014}. Since the removal of \ce{CO2} from the atmosphere via the carbon cycle is a very slow process, the \ce{CO2} released in the atmosphere through the burning of fossil fuels can stay there for tens of thousands of years \cite{Archer_2009}. Due to the combination of a very high emission rate, very long atmospheric life-time, and its greenhouse effects, the focus of much of the global warming mitigation effort and discussion has centered around the \ce{CO2} molecule. 

\medskip

A shift in the carbon cycle because of the higher atmospheric levels of \ce{CO2} results in several cascading effects that go beyond just increasing temperatures (Figure \ref{fgr:IPCC_impact}). These include: rising sea levels, ecosystem collapse, species extinction due to changing environmental conditions, threat to health, food, and water security of society, increased occurrence of extreme weather events and unpredictable weather patterns \cite{IPCC2014}.

\begin{figure}[ht]
\centering
\includegraphics[scale=0.85]{Introduction/figures/ipcc_impact.png}
\caption{Level of risk for natural and human systems for different levels of global warming. Figure reproduced from reference \citet[Figure~SPM.2]{IPCC2018}.}
\label{fgr:IPCC_impact}
\end{figure}

\medskip

\section{Mitigation options against climate change}

Since the industrial revolution, emissions of \ce{CO2} due to human activities have already resulted in a warming of around \ang{1}C in the global average surface temperature \cite{IPCC2018}. The \emph{Intergovernmental Panel on Climate Change} (IPCC) has described ways for limiting warming to \ang{1.5}C and the associated risks of climate change \cite{IPCC2018}. Figure \ref{fgr:IPCC_Tvst} shows forecasts of the global average surface temperature under multiple scenarios; the shaded portions in the graph show the forecast uncertainty. It is clear that a rapid decline of \ce{CO2} emissions over the next two to three decades to reach net-zero is necessary to prevent a catastrophic rise in the average global surface temperature. Additionally, emissions of other GHGs such as \ce{CH4}, \ce{N2O}, and aerosols also need to be curtailed significantly in this period.

\medskip

All proposed and projected routes to a \ang{1.5}C warmer world involve a higher share of electricity as the energy source for end use, with the majority of the electricity generation through renewable sources such as wind and solar \cite{IPCC2014}. The increasing share of renewable electricity reduces the intensity of both, \ce{CO2}, and methane emissions of the energy supply. Adoption of renewable energy is expected to be driven by favorable economics and other system-wide benefits in several parts of the world \cite{Grubler2018,Krey2014,GIELEN2019,Rogelj2018}. A higher share of wind and solar energy brings with it challenges of intermittency, and it needs simultaneous development of electricity storage technologies to improve the flexibility of electrical grids \cite{DENHOLM2011,Dunn2011,IRENA2020}. A lack of suitable and competitive electricity storage options can become a major bottleneck for the large-scale adoption of renewable electricity generation. 

\begin{figure}[p]
\centering
\includegraphics[width=\textwidth]{Introduction/figures/ipcc_T_vs_t.png}
\caption{Evolution of mean global surface temperature over time for multiple emission scenarios. Figure reproduced from reference \citet[Figure~SPM.1]{IPCC2018}.}
\label{fgr:IPCC_Tvst}
\end{figure}

\medskip

Industrial processes and transportation combined contribute \textasciitilde 45\% of the global GHG emissions \cite{IPCC2014}. Transitioning these sectors to reach net-zero emissions by 2040-2050 is a technologically and economically challenging task requiring major improvements in efficiency, electrification of manufacturing processes and substitution of current fossil fuel raw materials with sustainable ones \cite{IPCC2018,Bengt2017,BATAILLE2018}. Industry, in particular chemicals, cement, iron and steel manufacturing, aviation, shipping, and transportation sectors pose some of the hardest challenges towards decarbonisation as well as reduced non-\ce{CO2} emissions \cite{IRENA2020,IEA2019_CCUS}. The difficulty in reducing \ce{CO2} emissions in these sectors arises from factors such as requirement of high energy intensity, C-based building blocks for materials and processes which are hard to replace, and a growing demand for these industries for growth-driven economic activity \cite{IEA2019_CCUS}. \textit{Carbon dioxide removal} (CDR) in the order of 100-1000 Gt\ce{CO2} is considered as a potentially important mitigation measure to constrain atmospheric \ce{CO2} emissions from these sectors in order to meet the requirements for the \ang{1.5}C-\ang{2}C warming scenario \cite{IPCC2018,Minx_2018}.

\medskip

Figure \ref{fgr:CO2_fluxes} shows the estimated stocks of C on earth, the various CDR and \textit{carbon capture and utilization} (CCU) pathways, and the current flow of \ce{CO2} to and from the atmosphere. There is currently significant uncertainty in the large-scale and wide-spread deployment of CDR/CCU technologies due to the lack of policy incentives, challenges of upscaling, and potential trade-offs \cite{IPCC2018,Hepburn2019,Bardow2013}. Additionally, although reversing the average surface temperature rise in case of an overshoot beyond the \ang{1.5}C-\ang{2}C warming scenarios might be possible, the effects of the warming such as sea-level rise and ecological damage are not considered reversible \cite{Tokarska_2015}. Therefore, even though CDR will play an important role in complimenting other efforts to reach the net-zero \ce{CO2} emission target over the next three decades, extensive reliance on CDR as a mitigation mechanism for climate change is not desirable. 

\begin{figure}[p]
\centering
\includegraphics[scale=0.8]{Introduction/figures/hepburn_co2_fluxes.png}
\caption{Carbon dioxide removal and utilization pathways and net flows through the atmosphere. Red arrows are closed pathways signifying near-permanent removal of \ce{CO2}; yellow arrows are cycling pathways in which \ce{CO2} moves through industrial systems in time scales of days to months; purple arrows are open pathways signifying large \ce{CO2} removal potential but in potentially leaky systems. Blue block arrows represent annual fluxes of \ce{CO2} to and from the atmosphere averaged over the period of 2007-2018. The \ce{CO2} equivalent C stocks on earth are shown for atmosphere, biosphere, hydrosphere and lithosphere. Figure reproduced from reference \cite{Hepburn2019} with permission. Copyright 2019, Nature publishing.}
\label{fgr:CO2_fluxes}
\end{figure}

\medskip

\section{The case for electrochemical conversion of \ce{CO2}}

Utilizing \ce{CO2} as a feedstock for fuels, chemicals and building materials is becoming an increasingly competitive technology that can contribute to decarbonizing industrial and transportation sectors to meet emission reduction targets \cite{IEA2019_CO2,CHAUVY2019}. Conversion of \ce{CO2} using renewable energy can displace fossil-based feedstocks as a source of C to produce materials and chemicals \cite{Schlogl2010}. Life-cycle analysis (LCA) studies have shown that not only can \ce{CO2} conversion act as a sink to curtail emissions through long-lasting C-based products, production routes using \ce{CO2} as the source of C can diminish the footprint of certain manufacturing processes relative to the existing fossil-based routes \cite{Leitner2018,Bardow2013}. Another important conclusion from these studies is that CCU does not always imply CDR. LCA studies with the appropriate methodology for CCU pathways within their specific context are essential to establish their respective CDR potential. 

\medskip

The largest gains in emission reduction over the life-cycle of CCU processes can be made for the so-called Power-to-X (PtX) routes \cite{Leitner2018}. In a typical PtX scheme, renewable electricity is used to generate hydrogen (\ce{H2}) through electrochemical water-splitting, which is already a commercial technology and continues to become economically competitive \cite{IRENA2019_H2}. This renewable or so-called "green" \ce{H2} can be used directly in fuel cells or can be used in chemical processes where it reacts with captured \ce{CO2} to generate hydrocarbon molecules such as syngas, methane, methanol, Fischer-Tropsch (FT) liquids, dimethyl ether (DME), ethanol, etc., which can be fed directly into the existing fuels and chemicals value chain. Converting renewable electricity to chemical energy through PtX can help manage the intermittency of renewable electricity and can provide the necessary grid flexibility for high adaption of renewable electricity \cite{ZAKERI2015,ZHANG2015}. PtX therefore interacts with both, the electricity grid management, and the chemicals and fuels supply chain, to contribute to an integrated low-C energy and materials system.

\medskip

The integration of renewable energy and \ce{CO2} conversion can be done through multiple routes \cite{BAILERA2017,Ramirez2013} namely, photosynthesis \cite{RAZZAK2013,CHEAH2015}, photocatalysis \cite{Zou2014,Peng2016,Gong2016}, thermal catalysis \cite{GOTZ2016,Eichel2017,Leitner2018,BRYNOLF2018} or electrocatalysis \cite{Whipple2010,DeLuna2019,SMITH2019}. The electrochemical conversion route directly uses renewable electricity to reduce \ce{CO2} to products such as carbon monoxide, formate, methanol, methane, ethanol and ethylene \cite{Kortlever2015}. The process of electrochemical reduction of \ce{CO2} (e\ce{CO2}R) is therefore in-line with the PtX scheme, delivering system integration benefits of grid management and production of low-C hydrocarbon (LCHC) molecules. PtX using e\ce{CO2}R is sometimes termed as the direct route to LCHCs whereas electrochemically producing \ce{H2} to further react with \ce{CO2} to form LCHCs is termed as the indirect route. Figure \ref{fgr:flowchart_reactions} shows the pathways for the direct and indirect conversion of \ce{CO2} to industrially relevant hydrocarbon feedstocks. Considerable improvements have been realized in the performance of e\ce{CO2}R systems over the past few decades through accelerated academic and commercial R\&D efforts in the area. The following sections will briefly provide a state-of-the-art of the performance of current e\ce{CO2}R systems and the outlook for future developments in the field. 

\begin{figure}[h]
\centering
\includegraphics[scale=0.3]{Introduction/figures/intro_1_reactions.png}
\caption{Pathways to convert \ce{CO2} directly (in purple), or indirectly, via carbon monoxide (CO, in orange) and syngas (in blue) to chemical feedstocks. Pathways involving hydrogen (\ce{H2}) derived through water electrolysis are shown in green. Boxes with multiple colours imply a possibility of both a direct route and indirect route to production. The use cases for the reduction products are given in square brackets.}
\label{fgr:flowchart_reactions}
\end{figure}

\medskip

\section{Current status of \ce{CO2} electrocatalysis technology} \label{sec:current_status}

The reduction of \ce{CO2} takes place at the cathode surface which is kept at a negative potential using renewable electricity as the energy input. The cathode catalyst can be a d-block metal, a p-block metal or oxide, doped carbon-based materials, homogeneous molecular redox couple, chalcogenide or an enzyme \cite{Larrazbal2017}. The equation \ref{rxn:CO2-product} shows a general reaction for the reduction of \ce{CO2} in the presence of \ce{H2O} to form a product and hydroxide ions (\ce{OH-}) as a by-product, and using electrons for the thermodynamic driving force. 

\begin{equation}
\cee{xCO2(aq) + yH2O(aq) + ze^- <=> Product + qOH^{-}(aq)}
\label{rxn:CO2-product}
\end{equation}

\medskip

\begin{figure}[h]
\centering
\includegraphics[scale=0.35]{Introduction/figures/tom_ees.png}
\caption{Illustration of cell configurations for \ce{CO2} electrocatalysis. a) H-cell type configuration, b) GDE-based configuration with \ce{CO2} fed in gas phase with aqueous catholyte, c) GDE-based configuration with humidified \ce{CO2} fed in gas phase with solid supported catholyte, d) mass transport of \ce{CO2} from bulk of catholyte to the catalyst surface in a H-cell system configuration, e) mass transport of \ce{CO2} through the gas diffusion media to the catalyst surface in a GDE-based configuration. The anode reaction for all configurations a), b) and c) is shown as water (\ce{H2O}) oxidation to form \ce{O2} as an example. Figure reproduced from reference \cite{Tom_EES_2019}.}
\label{fgr:tom_ees}
\end{figure}

\medskip

Figure \ref{fgr:tom_ees} shows general schemes for an e\ce{CO2}R system. Research in e\ce{CO2}R systems has progressively moved from setups such as in Figure \ref{fgr:tom_ees}a where the electrolyte is saturated with \ce{CO2}, to setups like Figure \ref{fgr:tom_ees}b and \ref{fgr:tom_ees}c where the \ce{CO2} is fed to the system in vapour phase. The former is typically referred to as an H-cell type setup whereas the later is a gas diffusion electrode (GDE)-based system. GDE-based systems can achieve a significantly higher mass transport rate of \ce{CO2} to the catalyst surface by shortening the diffusion path of \ce{CO2} (Figure \ref{fgr:tom_ees}d \textit{vs.} \ref{fgr:tom_ees}e) and consequently show a considerably superior performance relative to H-cells \cite{Tom_EES_2019,Weber_GDEparadigm_2019,Fan2020,ENDRODI2017,Rufford_JMCA}.

\medskip

Techno-economic studies for e\ce{CO2}R systems have shown that a threshold current density, signifying the rate of \ce{CO2} reduction per catalyst surface area, in the range of 200 - 400 mA/cm$^2$ is needed to be reached for a commercially viable process \cite{Jiao_techeco,kibria2019,Kumar_2018,Verma2016}. This is in addition to requirements of high product selectivity, energy efficiency and a minimum catalyst stability in the order of thousands of hours. GDE-based setups have shown promising performance for reduction of \ce{CO2} to products such as carbon monoxide \cite{haas2018technical,verma2017insights,ma2016carbon,dufek2012operation}, formate \cite{whipple2010_formate,lu2017high,li2006development}, methanol \cite{sun2016molybdenum,zhang2018electrochemical}, ethanol \cite{li2014electroreduction,song2016high,liu2017selective,hoang2018nanoporous,zhuang2018steering} and ethylene \cite{Gewirth2017,dinh2018}. Products involving 2 electron transfers in the reduction reaction (z=2 in equation (\ref{rxn:CO2-product})), namely carbon monoxide (\ce{CO}), and formic acid (\ce{HCOOH}) are the closest to achieving economic feasibility based on current performance of lab setups \cite{Jiao_techeco,Verma2016,Bardow_2017}. Electrochemical production of syngas (\ce{CO} + \ce{H2}) coupled with the FT process to produce low-C diesel as aviation fuel or coupled with fermentation processes to produce high value chemicals are some of the promising directions for commercialization of e\ce{CO2}R \cite{Kumar_2018,DeLuna2019}. Producing formic acid using e\ce{CO2}R has a small climate change mitigation potential due to a relatively small global market, but the process can nonetheless act as a platform for the commercial development of the technology. Figure \ref{fgr:flowchart_reactions} shows a schematic of some of the possible routes to go from \ce{CO2} and \ce{H2O} to chemical feedstocks. Since e\ce{CO2}R systems typically employ aqueous electrolytes, reduction of \ce{H2O} (HER) competes with e\ce{CO2}R to form \ce{H2}, thereby reducing the selectivity of the desired products. Controlling the competition and interaction between the e\ce{CO2}R and HER reaction pathways is one of the challenges for designing high performing systems. Higher electron transfer e\ce{CO2}R reactions are accompanied with diminishing selectivity due to branching out of the reaction mechanisms and lower energy efficiency. However, significant progress has been made in the production of \ce{C_2} products ethylene (\ce{C2H4}) and ethanol (\ce{C2H5OH}) via e\ce{CO2}R over the past decade \cite{Fan2020,nitopi_chemrev}. e\ce{CO2}R to ethylene either directly or indirectly via carbon monoxide (Figure \ref{fgr:flowchart_reactions}) is an attractive avenue to pursue owing to a relatively large market size as well as \ce{CO2} emissions reduction potential \cite{DeLuna2019}. 

\medskip

\ce{CO2} utilization rates in the order of gigatons per annum need to be realized for a meaningful contribution to climate change mitigation targets (Figure \ref{fgr:CO2_fluxes}). Some of the biggest outstanding challenges in the path to commercial feasibility of e\ce{CO2}R have to do with the scaling-up of the electrolyzers in a cost-competitive manner. Current single-pass conversion yields are low and need to be increased for efficient implementation of downstream separation processes. Process conditions such as pressure and temperature used for studying and optimizing the performance of lab-scale e\ce{CO2}R systems need to approach the expected operating conditions at industrial scale based on integration schemes with upstream and downstream process units \cite{SMITH2019}. In the same vein, e\ce{CO2}R systems need to be tested for flexible on-demand operation if providing grid flexibility is one of the required functions. Catalyst stability has been identified as an important current bottleneck for long-term operation. In addition to developments in novel catalytic materials, improved cell architectures are expected to play an important role in achieving target performance thresholds of current density and energy efficiency \cite{Weber_GDEparadigm_2019,Berlinguette_2018}. Development of standardized protocols for experimental data acquisition and analysis will help compare the performance of various systems under investigation and will shorten the development cycle \cite{Bell_standards,Recep_defect}. Lastly, external factors like the unit price for low-C electricity and carbon pricing schemes will ultimately play a decisive role in determining the industrial applicability of the e\ce{CO2}R process.

\medskip

\section{Role of computational simulations}

e\ce{CO2}R is highly complex due to several coupled physical phenomena operating over vastly different length and time scales. Experimental analysis alone is often insufficient in isolating the influence of individual parameters on the observed performance of the system. Accessing the relevant time and length scales of the catalytic processes is also often not possible experimentally or requires extremely specialized and resource intensive infrastructure. Computational research has therefore become an important tool to address the gaps outlined in the previous section and is used widely to compliment and inform experimental studies.

\medskip

Simulations at the atomistic scale have been used extensively to derive the thermodynamic and kinetic energy barriers for reaction pathways as a means to understand the activity and selectivity of electrocatalysts towards desired products \cite{kibria2019,CALLEVALLEJO2012,Birdja2019}. Density functional theory (DFT)-based simulations have contributed tremendously in building the mechanistic understanding of the formation of \ce{C1} products such as \ce{CO} and formate (\ce{HCOO-}), as well as \ce{C2} and higher reduction products through \ce{C-C} coupling \cite{RENDONCALLE2018,Norskov_2010}. At the system scale, continuum mass transport models have proven very useful in understanding the reaction environment and engineering high performing device architectures for better yields \cite{Rufford_JMCA}. Studying the long-term stability of catalysts using simulations is a relatively under-developed area of research but is expected to become important considering its relevance for scale-up. Chapter \ref{intro_2} will go deeper into the physical phenomena relevant for e\ce{CO2}R and how simulations at multiple scales can be used to study and optimize these processes. 

\references{Introduction/intro}

