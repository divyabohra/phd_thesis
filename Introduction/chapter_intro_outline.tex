\chapter{Outline}
\label{intro_outline}

Electrochemical reduction of carbon dioxide (\ce{CO2}) is a promising technological solution for the realization of an integrated low-carbon energy and materials system. There are currently numerous outstanding challenges for the practical and large-scale application of \ce{CO2} electrocatalysis (e\ce{CO2}R). Computational simulations have an important role to play in developing the physical understanding to inform the design of optimized catalysts, interfaces and the overall process configuration. This thesis aims to address some of the critical research questions in e\ce{CO2}R through computational models at multiple scales.

\medskip

\section{Research Questions}

The results reported in this dissertation pertain to following research questions.

\begin{enumerate}
    \item \textit{Catalyst selectivity}: What determines the competition between two-electron \ce{CO2} reduction pathways on silver (Ag) electrocatalysts? To what extent do solvation and lateral adsorbate interactions influence the reaction energetics? Why do Ag catalysts produce carbon monoxide and not formate as a reduction product?
    
    \medskip

    \item \textit{Reaction interface}: What is the nature of the reaction environment at the catalyst-electrolyte interface at practically relevant potentials during e\ce{CO2}R? How can the properties of the interface influence the access of \ce{CO2} to the catalyst and the local pH? How important are electrostatic effects in defining the catalytic interface?
    
    \medskip

    \item \textit{Mass transport in practical systems}: To what extent do the structural and operational parameters of gas diffusion electrode (GDE)-based flow cells influence the reaction environment within the catalyst pores? What are the most important parameters determining the pH and concentration of ions and \ce{CO2} within the porous catalyst layer at practical current densities? 
\end{enumerate}

\medskip

\section{Chapter Outline}

The chapter-wise organization of the dissertation is given below.

\begin{itemize}
    \item Chapter \ref{intro_1} provides the context for electrochemical \ce{CO2} reduction and where the technology fits within the transition towards sustainable energy and materials. It provides an overview of the current status of the e\ce{CO2}R technology, the outstanding bottlenecks and the role of computational research within this scope.
    
    \medskip

    \item Chapter \ref{intro_2} provides a view of the e\ce{CO2}R modeling problem from three scales. Firstly, a surface catalysis view with atomic scale simulations used to model catalyst reactivity (section \ref{sec:catalyst}). Secondly, a catalyst-electrolyte interface view where the properties of the interface, as opposed to just the catalyst surface, are taken into account within the modeling approach (section \ref{sec:environment}). Thirdly, a device-scale view where mass transport becomes a decisive factor for catalytic performance with continuum models as tools for device engineering (section \ref{sec:transport}).
    
    \medskip

    \item Chapter \ref{angewandte} uses density functional theory (DFT)-based atomistic simulations to calculate the thermodynamic and kinetic energy barriers for 2-electron reduction products on high symmetry Ag surfaces. By using Bader charge analysis, implicit solvation, and accounting for lateral adsorbate interactions, we attempt to elucidate why Ag is selective towards carbon monoxide as opposed to formate.
    
    \medskip

    \item Chapter \ref{EES} uses a continuum one-dimensional generalized modified \\Poisson-Nernst-Planck (GMPNP) model to resolve the electrical double layer (EDL) in an e\ce{CO2}R system at steady state. The results of the GMPNP model are used to establish the importance of electrostatic forces and steric effects for the transport of \ce{CO2} to the catalyst surface, pH and the accumulation of cations at the interface.
    
    \medskip 
    
    \item Chapter \ref{chapter_GDE_pore} extends the GMPNP model from chapter \ref{EES} to a three-dimensional catalytic pore within a GDE-based flow-cell setup. The model is used to study the influence of operating conditions and structural parameters of the catalyst layer on the median reaction conditions within the pore. The chapter emphasizes the significance of the gas-electrolyte and electrolyte-electrolyte interfaces for determining reaction conditions in practical systems, in addition to the catalyst-electrolyte interface studied in the previous chapter.

    \medskip

    \item Chapter \ref{conclusion} offers future perspective in the context of the results presented in this thesis.

\end{itemize}

Chapters \ref{angewandte} and \ref{EES} are based on peer-reviewed journal articles whereas chapter \ref{chapter_GDE_pore} is based on the results currently under preparation for publication. 